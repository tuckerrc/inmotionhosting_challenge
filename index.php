 <?php 

require_once "vendor/autoload.php";
require_once "generated-conf/config.php";

?>

<!DOCTYPE html>
<html lang="en">

    <?php include("application/includes/head.php"); ?>

    <body>

	<?php include("application/includes/nav.php"); ?>
	
	<div class="container">
	    <h1>University of Southern Virginia Beach Dormitories</h1>
	    <?php
	    $units = UnitQuery::create()
			      ->orderByDormBuilding()
			      ->orderByFloorNumber()
	    		      ->orderByUnitNumber()
			      ->find();
	    $current_dorm = 1;
	    echo '<h2>Dorm 1</h2><div class="row">';
	    foreach($units as $unit){
		
		if ($unit->getDormBuilding() != $current_dorm){
		    echo '</div><h2>Dorm '. $unit->getDormBuilding() .'</h2><div class="row">';
		    $current_dorm = $unit->getDormBuilding();
		}

		if ($unit->getGender() == 0){
		    $gender = "M";
		} else {
		    $gender = "F";
		}
		$html = <<<HTML
<div class="unit text-center">
<div class="col-sm-3"><h3>Unit {$unit->getDormBuilding()}{$unit->getFloorNumber()}{$unit->getUnitNumber()}{$gender}</h3>
<div class="col-xs-5 bedroom" data-bedroom-id="{$unit->getBedroomOne()->getId()}">Bedroom 1</div>
<div class="col-xs-offset-2 col-xs-5 bedroom" data-bedroom-id="{$unit->getBedroomTwo()->getId()}">Bedroom 2</div>
<div class="col-xs-5 bedroom" data-bedroom-id="{$unit->getBedroomThree()->getId()}">Bedroom 3</div>
<div class="col-xs-offset-2 col-xs-5 bedroom" data-bedroom-id="{$unit->getBedroomFour()->getId()}">Bedroom 4</div>
<div class="col-xs-6 common">Common</div>
<div class="col-xs-6 kitchen">Kitchen</div>
</div>
</div>
HTML;

		echo $html;

	    }
	    echo '</div> <!-- /.row -->';
	    ?>
	</div><!-- /.container -->
	
	<?php include("application/includes/footer.php"); ?>

    </body>
</html>
