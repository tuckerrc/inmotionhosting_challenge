# University Southern Virginia Beach Dormitories

Simple application to manage two dormitories.

Instructions assume a working PHP/MySQL environment with composer

### Install Propel

_from propel documentation_

    $ wget http://getcomposer.org/composer.phar
    $ php composer.phar install
    $ ln -s vender/bin/propel propel
    $ composer dump-autoload

### Import database

MySQL commands

    CREATE DATABASE tucker_chapman;
    CREATE USER 'tchapman'@'localhost' IDENTIFIED BY 'tchapman';
    GRANT ALL PRIVILEGES ON tucker_chapman.* to 'tchapman'@'localhost';

bash command

    $ mysql -u tchapman --p -D tucker_chapman < sql/tucker_chapman.sql
	
run application

    $ php -S 127.0.0.1:4000
	
Application can now be accessed in browser by navigating to `localhost:4000`

### Create new Dorm Building 

Be sure the `$dorm_building_number` is the right number (I know not the best but I was rushed...) 

    $ php application/add_dormitory.php
	
There should now be an empty dormitory in the database
	
### Unfinished Features

- RUD - Cannot Remove, Update, or Delete students in GUI application - I wanted to implement a few other features
- No error checking for gender - Wanted to spend some time creating a GUI and did not get to this
- Cannot click on unit to see student and student info - This was the next thing I was going to work on
- Have not filled half rooms - I didn't have time to automate something and didn't want to spend time filling in manually
- The only classes and objects are those autocreated by Propel

### Completed Features

- Student Table has all necessary columns
- Allows you to Add new students
- Script to create new Dormitories with all necessary Units/Bedrooms
- MySQL for data storage
- Classes and Objects (Using Propel ORM)
- jQuery/Bootstrap using CDN
- No HTML Tables
- Runs in both Mozilla Firefox and Chrome
- DB Name and User/Pass noted above
- Graphical display of dorms

### TODO

- Some form of feedback on add student page
