<?php

namespace Base;

use \Bedroom as ChildBedroom;
use \BedroomQuery as ChildBedroomQuery;
use \UnitQuery as ChildUnitQuery;
use \Exception;
use \PDO;
use Map\UnitTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'unit' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Unit implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\UnitTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the dorm_building field.
     *
     * @var        int
     */
    protected $dorm_building;

    /**
     * The value for the unit_number field.
     *
     * @var        int
     */
    protected $unit_number;

    /**
     * The value for the floor_number field.
     *
     * @var        int
     */
    protected $floor_number;

    /**
     * The value for the gender field.
     *
     * @var        int
     */
    protected $gender;

    /**
     * The value for the bedroom1 field.
     *
     * @var        int
     */
    protected $bedroom1;

    /**
     * The value for the bedroom2 field.
     *
     * @var        int
     */
    protected $bedroom2;

    /**
     * The value for the bedroom3 field.
     *
     * @var        int
     */
    protected $bedroom3;

    /**
     * The value for the bedroom4 field.
     *
     * @var        int
     */
    protected $bedroom4;

    /**
     * @var        ChildBedroom
     */
    protected $aBedroomOne;

    /**
     * @var        ChildBedroom
     */
    protected $aBedroomTwo;

    /**
     * @var        ChildBedroom
     */
    protected $aBedroomThree;

    /**
     * @var        ChildBedroom
     */
    protected $aBedroomFour;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of Base\Unit object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Unit</code> instance.  If
     * <code>obj</code> is an instance of <code>Unit</code>, delegates to
     * <code>equals(Unit)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Unit The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [dorm_building] column value.
     *
     * @return int
     */
    public function getDormBuilding()
    {
        return $this->dorm_building;
    }

    /**
     * Get the [unit_number] column value.
     *
     * @return int
     */
    public function getUnitNumber()
    {
        return $this->unit_number;
    }

    /**
     * Get the [floor_number] column value.
     *
     * @return int
     */
    public function getFloorNumber()
    {
        return $this->floor_number;
    }

    /**
     * Get the [gender] column value.
     *
     * @return int
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Get the [bedroom1] column value.
     *
     * @return int
     */
    public function getBedroom1()
    {
        return $this->bedroom1;
    }

    /**
     * Get the [bedroom2] column value.
     *
     * @return int
     */
    public function getBedroom2()
    {
        return $this->bedroom2;
    }

    /**
     * Get the [bedroom3] column value.
     *
     * @return int
     */
    public function getBedroom3()
    {
        return $this->bedroom3;
    }

    /**
     * Get the [bedroom4] column value.
     *
     * @return int
     */
    public function getBedroom4()
    {
        return $this->bedroom4;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Unit The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[UnitTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [dorm_building] column.
     *
     * @param int $v new value
     * @return $this|\Unit The current object (for fluent API support)
     */
    public function setDormBuilding($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->dorm_building !== $v) {
            $this->dorm_building = $v;
            $this->modifiedColumns[UnitTableMap::COL_DORM_BUILDING] = true;
        }

        return $this;
    } // setDormBuilding()

    /**
     * Set the value of [unit_number] column.
     *
     * @param int $v new value
     * @return $this|\Unit The current object (for fluent API support)
     */
    public function setUnitNumber($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->unit_number !== $v) {
            $this->unit_number = $v;
            $this->modifiedColumns[UnitTableMap::COL_UNIT_NUMBER] = true;
        }

        return $this;
    } // setUnitNumber()

    /**
     * Set the value of [floor_number] column.
     *
     * @param int $v new value
     * @return $this|\Unit The current object (for fluent API support)
     */
    public function setFloorNumber($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->floor_number !== $v) {
            $this->floor_number = $v;
            $this->modifiedColumns[UnitTableMap::COL_FLOOR_NUMBER] = true;
        }

        return $this;
    } // setFloorNumber()

    /**
     * Set the value of [gender] column.
     *
     * @param int $v new value
     * @return $this|\Unit The current object (for fluent API support)
     */
    public function setGender($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->gender !== $v) {
            $this->gender = $v;
            $this->modifiedColumns[UnitTableMap::COL_GENDER] = true;
        }

        return $this;
    } // setGender()

    /**
     * Set the value of [bedroom1] column.
     *
     * @param int $v new value
     * @return $this|\Unit The current object (for fluent API support)
     */
    public function setBedroom1($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->bedroom1 !== $v) {
            $this->bedroom1 = $v;
            $this->modifiedColumns[UnitTableMap::COL_BEDROOM1] = true;
        }

        if ($this->aBedroomOne !== null && $this->aBedroomOne->getId() !== $v) {
            $this->aBedroomOne = null;
        }

        return $this;
    } // setBedroom1()

    /**
     * Set the value of [bedroom2] column.
     *
     * @param int $v new value
     * @return $this|\Unit The current object (for fluent API support)
     */
    public function setBedroom2($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->bedroom2 !== $v) {
            $this->bedroom2 = $v;
            $this->modifiedColumns[UnitTableMap::COL_BEDROOM2] = true;
        }

        if ($this->aBedroomTwo !== null && $this->aBedroomTwo->getId() !== $v) {
            $this->aBedroomTwo = null;
        }

        return $this;
    } // setBedroom2()

    /**
     * Set the value of [bedroom3] column.
     *
     * @param int $v new value
     * @return $this|\Unit The current object (for fluent API support)
     */
    public function setBedroom3($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->bedroom3 !== $v) {
            $this->bedroom3 = $v;
            $this->modifiedColumns[UnitTableMap::COL_BEDROOM3] = true;
        }

        if ($this->aBedroomThree !== null && $this->aBedroomThree->getId() !== $v) {
            $this->aBedroomThree = null;
        }

        return $this;
    } // setBedroom3()

    /**
     * Set the value of [bedroom4] column.
     *
     * @param int $v new value
     * @return $this|\Unit The current object (for fluent API support)
     */
    public function setBedroom4($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->bedroom4 !== $v) {
            $this->bedroom4 = $v;
            $this->modifiedColumns[UnitTableMap::COL_BEDROOM4] = true;
        }

        if ($this->aBedroomFour !== null && $this->aBedroomFour->getId() !== $v) {
            $this->aBedroomFour = null;
        }

        return $this;
    } // setBedroom4()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : UnitTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : UnitTableMap::translateFieldName('DormBuilding', TableMap::TYPE_PHPNAME, $indexType)];
            $this->dorm_building = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : UnitTableMap::translateFieldName('UnitNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->unit_number = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : UnitTableMap::translateFieldName('FloorNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->floor_number = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : UnitTableMap::translateFieldName('Gender', TableMap::TYPE_PHPNAME, $indexType)];
            $this->gender = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : UnitTableMap::translateFieldName('Bedroom1', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bedroom1 = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : UnitTableMap::translateFieldName('Bedroom2', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bedroom2 = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : UnitTableMap::translateFieldName('Bedroom3', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bedroom3 = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : UnitTableMap::translateFieldName('Bedroom4', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bedroom4 = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 9; // 9 = UnitTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Unit'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aBedroomOne !== null && $this->bedroom1 !== $this->aBedroomOne->getId()) {
            $this->aBedroomOne = null;
        }
        if ($this->aBedroomTwo !== null && $this->bedroom2 !== $this->aBedroomTwo->getId()) {
            $this->aBedroomTwo = null;
        }
        if ($this->aBedroomThree !== null && $this->bedroom3 !== $this->aBedroomThree->getId()) {
            $this->aBedroomThree = null;
        }
        if ($this->aBedroomFour !== null && $this->bedroom4 !== $this->aBedroomFour->getId()) {
            $this->aBedroomFour = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UnitTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildUnitQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aBedroomOne = null;
            $this->aBedroomTwo = null;
            $this->aBedroomThree = null;
            $this->aBedroomFour = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Unit::setDeleted()
     * @see Unit::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UnitTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildUnitQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UnitTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                UnitTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aBedroomOne !== null) {
                if ($this->aBedroomOne->isModified() || $this->aBedroomOne->isNew()) {
                    $affectedRows += $this->aBedroomOne->save($con);
                }
                $this->setBedroomOne($this->aBedroomOne);
            }

            if ($this->aBedroomTwo !== null) {
                if ($this->aBedroomTwo->isModified() || $this->aBedroomTwo->isNew()) {
                    $affectedRows += $this->aBedroomTwo->save($con);
                }
                $this->setBedroomTwo($this->aBedroomTwo);
            }

            if ($this->aBedroomThree !== null) {
                if ($this->aBedroomThree->isModified() || $this->aBedroomThree->isNew()) {
                    $affectedRows += $this->aBedroomThree->save($con);
                }
                $this->setBedroomThree($this->aBedroomThree);
            }

            if ($this->aBedroomFour !== null) {
                if ($this->aBedroomFour->isModified() || $this->aBedroomFour->isNew()) {
                    $affectedRows += $this->aBedroomFour->save($con);
                }
                $this->setBedroomFour($this->aBedroomFour);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[UnitTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . UnitTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(UnitTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(UnitTableMap::COL_DORM_BUILDING)) {
            $modifiedColumns[':p' . $index++]  = 'dorm_building';
        }
        if ($this->isColumnModified(UnitTableMap::COL_UNIT_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'unit_number';
        }
        if ($this->isColumnModified(UnitTableMap::COL_FLOOR_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'floor_number';
        }
        if ($this->isColumnModified(UnitTableMap::COL_GENDER)) {
            $modifiedColumns[':p' . $index++]  = 'gender';
        }
        if ($this->isColumnModified(UnitTableMap::COL_BEDROOM1)) {
            $modifiedColumns[':p' . $index++]  = 'bedroom1';
        }
        if ($this->isColumnModified(UnitTableMap::COL_BEDROOM2)) {
            $modifiedColumns[':p' . $index++]  = 'bedroom2';
        }
        if ($this->isColumnModified(UnitTableMap::COL_BEDROOM3)) {
            $modifiedColumns[':p' . $index++]  = 'bedroom3';
        }
        if ($this->isColumnModified(UnitTableMap::COL_BEDROOM4)) {
            $modifiedColumns[':p' . $index++]  = 'bedroom4';
        }

        $sql = sprintf(
            'INSERT INTO unit (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'dorm_building':
                        $stmt->bindValue($identifier, $this->dorm_building, PDO::PARAM_INT);
                        break;
                    case 'unit_number':
                        $stmt->bindValue($identifier, $this->unit_number, PDO::PARAM_INT);
                        break;
                    case 'floor_number':
                        $stmt->bindValue($identifier, $this->floor_number, PDO::PARAM_INT);
                        break;
                    case 'gender':
                        $stmt->bindValue($identifier, $this->gender, PDO::PARAM_INT);
                        break;
                    case 'bedroom1':
                        $stmt->bindValue($identifier, $this->bedroom1, PDO::PARAM_INT);
                        break;
                    case 'bedroom2':
                        $stmt->bindValue($identifier, $this->bedroom2, PDO::PARAM_INT);
                        break;
                    case 'bedroom3':
                        $stmt->bindValue($identifier, $this->bedroom3, PDO::PARAM_INT);
                        break;
                    case 'bedroom4':
                        $stmt->bindValue($identifier, $this->bedroom4, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UnitTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getDormBuilding();
                break;
            case 2:
                return $this->getUnitNumber();
                break;
            case 3:
                return $this->getFloorNumber();
                break;
            case 4:
                return $this->getGender();
                break;
            case 5:
                return $this->getBedroom1();
                break;
            case 6:
                return $this->getBedroom2();
                break;
            case 7:
                return $this->getBedroom3();
                break;
            case 8:
                return $this->getBedroom4();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Unit'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Unit'][$this->hashCode()] = true;
        $keys = UnitTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getDormBuilding(),
            $keys[2] => $this->getUnitNumber(),
            $keys[3] => $this->getFloorNumber(),
            $keys[4] => $this->getGender(),
            $keys[5] => $this->getBedroom1(),
            $keys[6] => $this->getBedroom2(),
            $keys[7] => $this->getBedroom3(),
            $keys[8] => $this->getBedroom4(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aBedroomOne) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'bedroom';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'bedroom';
                        break;
                    default:
                        $key = 'BedroomOne';
                }

                $result[$key] = $this->aBedroomOne->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aBedroomTwo) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'bedroom';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'bedroom';
                        break;
                    default:
                        $key = 'BedroomTwo';
                }

                $result[$key] = $this->aBedroomTwo->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aBedroomThree) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'bedroom';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'bedroom';
                        break;
                    default:
                        $key = 'BedroomThree';
                }

                $result[$key] = $this->aBedroomThree->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aBedroomFour) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'bedroom';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'bedroom';
                        break;
                    default:
                        $key = 'BedroomFour';
                }

                $result[$key] = $this->aBedroomFour->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Unit
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UnitTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Unit
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setDormBuilding($value);
                break;
            case 2:
                $this->setUnitNumber($value);
                break;
            case 3:
                $this->setFloorNumber($value);
                break;
            case 4:
                $this->setGender($value);
                break;
            case 5:
                $this->setBedroom1($value);
                break;
            case 6:
                $this->setBedroom2($value);
                break;
            case 7:
                $this->setBedroom3($value);
                break;
            case 8:
                $this->setBedroom4($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = UnitTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setDormBuilding($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setUnitNumber($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setFloorNumber($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setGender($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setBedroom1($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setBedroom2($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setBedroom3($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setBedroom4($arr[$keys[8]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Unit The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(UnitTableMap::DATABASE_NAME);

        if ($this->isColumnModified(UnitTableMap::COL_ID)) {
            $criteria->add(UnitTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(UnitTableMap::COL_DORM_BUILDING)) {
            $criteria->add(UnitTableMap::COL_DORM_BUILDING, $this->dorm_building);
        }
        if ($this->isColumnModified(UnitTableMap::COL_UNIT_NUMBER)) {
            $criteria->add(UnitTableMap::COL_UNIT_NUMBER, $this->unit_number);
        }
        if ($this->isColumnModified(UnitTableMap::COL_FLOOR_NUMBER)) {
            $criteria->add(UnitTableMap::COL_FLOOR_NUMBER, $this->floor_number);
        }
        if ($this->isColumnModified(UnitTableMap::COL_GENDER)) {
            $criteria->add(UnitTableMap::COL_GENDER, $this->gender);
        }
        if ($this->isColumnModified(UnitTableMap::COL_BEDROOM1)) {
            $criteria->add(UnitTableMap::COL_BEDROOM1, $this->bedroom1);
        }
        if ($this->isColumnModified(UnitTableMap::COL_BEDROOM2)) {
            $criteria->add(UnitTableMap::COL_BEDROOM2, $this->bedroom2);
        }
        if ($this->isColumnModified(UnitTableMap::COL_BEDROOM3)) {
            $criteria->add(UnitTableMap::COL_BEDROOM3, $this->bedroom3);
        }
        if ($this->isColumnModified(UnitTableMap::COL_BEDROOM4)) {
            $criteria->add(UnitTableMap::COL_BEDROOM4, $this->bedroom4);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildUnitQuery::create();
        $criteria->add(UnitTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Unit (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setDormBuilding($this->getDormBuilding());
        $copyObj->setUnitNumber($this->getUnitNumber());
        $copyObj->setFloorNumber($this->getFloorNumber());
        $copyObj->setGender($this->getGender());
        $copyObj->setBedroom1($this->getBedroom1());
        $copyObj->setBedroom2($this->getBedroom2());
        $copyObj->setBedroom3($this->getBedroom3());
        $copyObj->setBedroom4($this->getBedroom4());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Unit Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildBedroom object.
     *
     * @param  ChildBedroom $v
     * @return $this|\Unit The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBedroomOne(ChildBedroom $v = null)
    {
        if ($v === null) {
            $this->setBedroom1(NULL);
        } else {
            $this->setBedroom1($v->getId());
        }

        $this->aBedroomOne = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildBedroom object, it will not be re-added.
        if ($v !== null) {
            $v->addBedroomOne($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildBedroom object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildBedroom The associated ChildBedroom object.
     * @throws PropelException
     */
    public function getBedroomOne(ConnectionInterface $con = null)
    {
        if ($this->aBedroomOne === null && ($this->bedroom1 !== null)) {
            $this->aBedroomOne = ChildBedroomQuery::create()->findPk($this->bedroom1, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBedroomOne->addBedroomOnes($this);
             */
        }

        return $this->aBedroomOne;
    }

    /**
     * Declares an association between this object and a ChildBedroom object.
     *
     * @param  ChildBedroom $v
     * @return $this|\Unit The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBedroomTwo(ChildBedroom $v = null)
    {
        if ($v === null) {
            $this->setBedroom2(NULL);
        } else {
            $this->setBedroom2($v->getId());
        }

        $this->aBedroomTwo = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildBedroom object, it will not be re-added.
        if ($v !== null) {
            $v->addBedroomTwo($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildBedroom object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildBedroom The associated ChildBedroom object.
     * @throws PropelException
     */
    public function getBedroomTwo(ConnectionInterface $con = null)
    {
        if ($this->aBedroomTwo === null && ($this->bedroom2 !== null)) {
            $this->aBedroomTwo = ChildBedroomQuery::create()->findPk($this->bedroom2, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBedroomTwo->addBedroomTwos($this);
             */
        }

        return $this->aBedroomTwo;
    }

    /**
     * Declares an association between this object and a ChildBedroom object.
     *
     * @param  ChildBedroom $v
     * @return $this|\Unit The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBedroomThree(ChildBedroom $v = null)
    {
        if ($v === null) {
            $this->setBedroom3(NULL);
        } else {
            $this->setBedroom3($v->getId());
        }

        $this->aBedroomThree = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildBedroom object, it will not be re-added.
        if ($v !== null) {
            $v->addBedroomThree($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildBedroom object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildBedroom The associated ChildBedroom object.
     * @throws PropelException
     */
    public function getBedroomThree(ConnectionInterface $con = null)
    {
        if ($this->aBedroomThree === null && ($this->bedroom3 !== null)) {
            $this->aBedroomThree = ChildBedroomQuery::create()->findPk($this->bedroom3, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBedroomThree->addBedroomThrees($this);
             */
        }

        return $this->aBedroomThree;
    }

    /**
     * Declares an association between this object and a ChildBedroom object.
     *
     * @param  ChildBedroom $v
     * @return $this|\Unit The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBedroomFour(ChildBedroom $v = null)
    {
        if ($v === null) {
            $this->setBedroom4(NULL);
        } else {
            $this->setBedroom4($v->getId());
        }

        $this->aBedroomFour = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildBedroom object, it will not be re-added.
        if ($v !== null) {
            $v->addBedroomFour($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildBedroom object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildBedroom The associated ChildBedroom object.
     * @throws PropelException
     */
    public function getBedroomFour(ConnectionInterface $con = null)
    {
        if ($this->aBedroomFour === null && ($this->bedroom4 !== null)) {
            $this->aBedroomFour = ChildBedroomQuery::create()->findPk($this->bedroom4, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBedroomFour->addBedroomFours($this);
             */
        }

        return $this->aBedroomFour;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aBedroomOne) {
            $this->aBedroomOne->removeBedroomOne($this);
        }
        if (null !== $this->aBedroomTwo) {
            $this->aBedroomTwo->removeBedroomTwo($this);
        }
        if (null !== $this->aBedroomThree) {
            $this->aBedroomThree->removeBedroomThree($this);
        }
        if (null !== $this->aBedroomFour) {
            $this->aBedroomFour->removeBedroomFour($this);
        }
        $this->id = null;
        $this->dorm_building = null;
        $this->unit_number = null;
        $this->floor_number = null;
        $this->gender = null;
        $this->bedroom1 = null;
        $this->bedroom2 = null;
        $this->bedroom3 = null;
        $this->bedroom4 = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aBedroomOne = null;
        $this->aBedroomTwo = null;
        $this->aBedroomThree = null;
        $this->aBedroomFour = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(UnitTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
