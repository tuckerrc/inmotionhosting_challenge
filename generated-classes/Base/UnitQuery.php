<?php

namespace Base;

use \Unit as ChildUnit;
use \UnitQuery as ChildUnitQuery;
use \Exception;
use \PDO;
use Map\UnitTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'unit' table.
 *
 *
 *
 * @method     ChildUnitQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUnitQuery orderByDormBuilding($order = Criteria::ASC) Order by the dorm_building column
 * @method     ChildUnitQuery orderByUnitNumber($order = Criteria::ASC) Order by the unit_number column
 * @method     ChildUnitQuery orderByFloorNumber($order = Criteria::ASC) Order by the floor_number column
 * @method     ChildUnitQuery orderByGender($order = Criteria::ASC) Order by the gender column
 * @method     ChildUnitQuery orderByBedroom1($order = Criteria::ASC) Order by the bedroom1 column
 * @method     ChildUnitQuery orderByBedroom2($order = Criteria::ASC) Order by the bedroom2 column
 * @method     ChildUnitQuery orderByBedroom3($order = Criteria::ASC) Order by the bedroom3 column
 * @method     ChildUnitQuery orderByBedroom4($order = Criteria::ASC) Order by the bedroom4 column
 *
 * @method     ChildUnitQuery groupById() Group by the id column
 * @method     ChildUnitQuery groupByDormBuilding() Group by the dorm_building column
 * @method     ChildUnitQuery groupByUnitNumber() Group by the unit_number column
 * @method     ChildUnitQuery groupByFloorNumber() Group by the floor_number column
 * @method     ChildUnitQuery groupByGender() Group by the gender column
 * @method     ChildUnitQuery groupByBedroom1() Group by the bedroom1 column
 * @method     ChildUnitQuery groupByBedroom2() Group by the bedroom2 column
 * @method     ChildUnitQuery groupByBedroom3() Group by the bedroom3 column
 * @method     ChildUnitQuery groupByBedroom4() Group by the bedroom4 column
 *
 * @method     ChildUnitQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUnitQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUnitQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUnitQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUnitQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUnitQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUnitQuery leftJoinBedroomOne($relationAlias = null) Adds a LEFT JOIN clause to the query using the BedroomOne relation
 * @method     ChildUnitQuery rightJoinBedroomOne($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BedroomOne relation
 * @method     ChildUnitQuery innerJoinBedroomOne($relationAlias = null) Adds a INNER JOIN clause to the query using the BedroomOne relation
 *
 * @method     ChildUnitQuery joinWithBedroomOne($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BedroomOne relation
 *
 * @method     ChildUnitQuery leftJoinWithBedroomOne() Adds a LEFT JOIN clause and with to the query using the BedroomOne relation
 * @method     ChildUnitQuery rightJoinWithBedroomOne() Adds a RIGHT JOIN clause and with to the query using the BedroomOne relation
 * @method     ChildUnitQuery innerJoinWithBedroomOne() Adds a INNER JOIN clause and with to the query using the BedroomOne relation
 *
 * @method     ChildUnitQuery leftJoinBedroomTwo($relationAlias = null) Adds a LEFT JOIN clause to the query using the BedroomTwo relation
 * @method     ChildUnitQuery rightJoinBedroomTwo($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BedroomTwo relation
 * @method     ChildUnitQuery innerJoinBedroomTwo($relationAlias = null) Adds a INNER JOIN clause to the query using the BedroomTwo relation
 *
 * @method     ChildUnitQuery joinWithBedroomTwo($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BedroomTwo relation
 *
 * @method     ChildUnitQuery leftJoinWithBedroomTwo() Adds a LEFT JOIN clause and with to the query using the BedroomTwo relation
 * @method     ChildUnitQuery rightJoinWithBedroomTwo() Adds a RIGHT JOIN clause and with to the query using the BedroomTwo relation
 * @method     ChildUnitQuery innerJoinWithBedroomTwo() Adds a INNER JOIN clause and with to the query using the BedroomTwo relation
 *
 * @method     ChildUnitQuery leftJoinBedroomThree($relationAlias = null) Adds a LEFT JOIN clause to the query using the BedroomThree relation
 * @method     ChildUnitQuery rightJoinBedroomThree($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BedroomThree relation
 * @method     ChildUnitQuery innerJoinBedroomThree($relationAlias = null) Adds a INNER JOIN clause to the query using the BedroomThree relation
 *
 * @method     ChildUnitQuery joinWithBedroomThree($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BedroomThree relation
 *
 * @method     ChildUnitQuery leftJoinWithBedroomThree() Adds a LEFT JOIN clause and with to the query using the BedroomThree relation
 * @method     ChildUnitQuery rightJoinWithBedroomThree() Adds a RIGHT JOIN clause and with to the query using the BedroomThree relation
 * @method     ChildUnitQuery innerJoinWithBedroomThree() Adds a INNER JOIN clause and with to the query using the BedroomThree relation
 *
 * @method     ChildUnitQuery leftJoinBedroomFour($relationAlias = null) Adds a LEFT JOIN clause to the query using the BedroomFour relation
 * @method     ChildUnitQuery rightJoinBedroomFour($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BedroomFour relation
 * @method     ChildUnitQuery innerJoinBedroomFour($relationAlias = null) Adds a INNER JOIN clause to the query using the BedroomFour relation
 *
 * @method     ChildUnitQuery joinWithBedroomFour($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BedroomFour relation
 *
 * @method     ChildUnitQuery leftJoinWithBedroomFour() Adds a LEFT JOIN clause and with to the query using the BedroomFour relation
 * @method     ChildUnitQuery rightJoinWithBedroomFour() Adds a RIGHT JOIN clause and with to the query using the BedroomFour relation
 * @method     ChildUnitQuery innerJoinWithBedroomFour() Adds a INNER JOIN clause and with to the query using the BedroomFour relation
 *
 * @method     \BedroomQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUnit findOne(ConnectionInterface $con = null) Return the first ChildUnit matching the query
 * @method     ChildUnit findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUnit matching the query, or a new ChildUnit object populated from the query conditions when no match is found
 *
 * @method     ChildUnit findOneById(int $id) Return the first ChildUnit filtered by the id column
 * @method     ChildUnit findOneByDormBuilding(int $dorm_building) Return the first ChildUnit filtered by the dorm_building column
 * @method     ChildUnit findOneByUnitNumber(int $unit_number) Return the first ChildUnit filtered by the unit_number column
 * @method     ChildUnit findOneByFloorNumber(int $floor_number) Return the first ChildUnit filtered by the floor_number column
 * @method     ChildUnit findOneByGender(int $gender) Return the first ChildUnit filtered by the gender column
 * @method     ChildUnit findOneByBedroom1(int $bedroom1) Return the first ChildUnit filtered by the bedroom1 column
 * @method     ChildUnit findOneByBedroom2(int $bedroom2) Return the first ChildUnit filtered by the bedroom2 column
 * @method     ChildUnit findOneByBedroom3(int $bedroom3) Return the first ChildUnit filtered by the bedroom3 column
 * @method     ChildUnit findOneByBedroom4(int $bedroom4) Return the first ChildUnit filtered by the bedroom4 column *

 * @method     ChildUnit requirePk($key, ConnectionInterface $con = null) Return the ChildUnit by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnit requireOne(ConnectionInterface $con = null) Return the first ChildUnit matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUnit requireOneById(int $id) Return the first ChildUnit filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnit requireOneByDormBuilding(int $dorm_building) Return the first ChildUnit filtered by the dorm_building column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnit requireOneByUnitNumber(int $unit_number) Return the first ChildUnit filtered by the unit_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnit requireOneByFloorNumber(int $floor_number) Return the first ChildUnit filtered by the floor_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnit requireOneByGender(int $gender) Return the first ChildUnit filtered by the gender column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnit requireOneByBedroom1(int $bedroom1) Return the first ChildUnit filtered by the bedroom1 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnit requireOneByBedroom2(int $bedroom2) Return the first ChildUnit filtered by the bedroom2 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnit requireOneByBedroom3(int $bedroom3) Return the first ChildUnit filtered by the bedroom3 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnit requireOneByBedroom4(int $bedroom4) Return the first ChildUnit filtered by the bedroom4 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUnit[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUnit objects based on current ModelCriteria
 * @method     ChildUnit[]|ObjectCollection findById(int $id) Return ChildUnit objects filtered by the id column
 * @method     ChildUnit[]|ObjectCollection findByDormBuilding(int $dorm_building) Return ChildUnit objects filtered by the dorm_building column
 * @method     ChildUnit[]|ObjectCollection findByUnitNumber(int $unit_number) Return ChildUnit objects filtered by the unit_number column
 * @method     ChildUnit[]|ObjectCollection findByFloorNumber(int $floor_number) Return ChildUnit objects filtered by the floor_number column
 * @method     ChildUnit[]|ObjectCollection findByGender(int $gender) Return ChildUnit objects filtered by the gender column
 * @method     ChildUnit[]|ObjectCollection findByBedroom1(int $bedroom1) Return ChildUnit objects filtered by the bedroom1 column
 * @method     ChildUnit[]|ObjectCollection findByBedroom2(int $bedroom2) Return ChildUnit objects filtered by the bedroom2 column
 * @method     ChildUnit[]|ObjectCollection findByBedroom3(int $bedroom3) Return ChildUnit objects filtered by the bedroom3 column
 * @method     ChildUnit[]|ObjectCollection findByBedroom4(int $bedroom4) Return ChildUnit objects filtered by the bedroom4 column
 * @method     ChildUnit[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UnitQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\UnitQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'tucker_chapman', $modelName = '\\Unit', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUnitQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUnitQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUnitQuery) {
            return $criteria;
        }
        $query = new ChildUnitQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUnit|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UnitTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UnitTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUnit A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, dorm_building, unit_number, floor_number, gender, bedroom1, bedroom2, bedroom3, bedroom4 FROM unit WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUnit $obj */
            $obj = new ChildUnit();
            $obj->hydrate($row);
            UnitTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUnit|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUnitQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UnitTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUnitQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UnitTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UnitTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UnitTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UnitTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the dorm_building column
     *
     * Example usage:
     * <code>
     * $query->filterByDormBuilding(1234); // WHERE dorm_building = 1234
     * $query->filterByDormBuilding(array(12, 34)); // WHERE dorm_building IN (12, 34)
     * $query->filterByDormBuilding(array('min' => 12)); // WHERE dorm_building > 12
     * </code>
     *
     * @param     mixed $dormBuilding The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitQuery The current query, for fluid interface
     */
    public function filterByDormBuilding($dormBuilding = null, $comparison = null)
    {
        if (is_array($dormBuilding)) {
            $useMinMax = false;
            if (isset($dormBuilding['min'])) {
                $this->addUsingAlias(UnitTableMap::COL_DORM_BUILDING, $dormBuilding['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dormBuilding['max'])) {
                $this->addUsingAlias(UnitTableMap::COL_DORM_BUILDING, $dormBuilding['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UnitTableMap::COL_DORM_BUILDING, $dormBuilding, $comparison);
    }

    /**
     * Filter the query on the unit_number column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitNumber(1234); // WHERE unit_number = 1234
     * $query->filterByUnitNumber(array(12, 34)); // WHERE unit_number IN (12, 34)
     * $query->filterByUnitNumber(array('min' => 12)); // WHERE unit_number > 12
     * </code>
     *
     * @param     mixed $unitNumber The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitQuery The current query, for fluid interface
     */
    public function filterByUnitNumber($unitNumber = null, $comparison = null)
    {
        if (is_array($unitNumber)) {
            $useMinMax = false;
            if (isset($unitNumber['min'])) {
                $this->addUsingAlias(UnitTableMap::COL_UNIT_NUMBER, $unitNumber['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($unitNumber['max'])) {
                $this->addUsingAlias(UnitTableMap::COL_UNIT_NUMBER, $unitNumber['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UnitTableMap::COL_UNIT_NUMBER, $unitNumber, $comparison);
    }

    /**
     * Filter the query on the floor_number column
     *
     * Example usage:
     * <code>
     * $query->filterByFloorNumber(1234); // WHERE floor_number = 1234
     * $query->filterByFloorNumber(array(12, 34)); // WHERE floor_number IN (12, 34)
     * $query->filterByFloorNumber(array('min' => 12)); // WHERE floor_number > 12
     * </code>
     *
     * @param     mixed $floorNumber The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitQuery The current query, for fluid interface
     */
    public function filterByFloorNumber($floorNumber = null, $comparison = null)
    {
        if (is_array($floorNumber)) {
            $useMinMax = false;
            if (isset($floorNumber['min'])) {
                $this->addUsingAlias(UnitTableMap::COL_FLOOR_NUMBER, $floorNumber['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($floorNumber['max'])) {
                $this->addUsingAlias(UnitTableMap::COL_FLOOR_NUMBER, $floorNumber['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UnitTableMap::COL_FLOOR_NUMBER, $floorNumber, $comparison);
    }

    /**
     * Filter the query on the gender column
     *
     * Example usage:
     * <code>
     * $query->filterByGender(1234); // WHERE gender = 1234
     * $query->filterByGender(array(12, 34)); // WHERE gender IN (12, 34)
     * $query->filterByGender(array('min' => 12)); // WHERE gender > 12
     * </code>
     *
     * @param     mixed $gender The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitQuery The current query, for fluid interface
     */
    public function filterByGender($gender = null, $comparison = null)
    {
        if (is_array($gender)) {
            $useMinMax = false;
            if (isset($gender['min'])) {
                $this->addUsingAlias(UnitTableMap::COL_GENDER, $gender['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($gender['max'])) {
                $this->addUsingAlias(UnitTableMap::COL_GENDER, $gender['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UnitTableMap::COL_GENDER, $gender, $comparison);
    }

    /**
     * Filter the query on the bedroom1 column
     *
     * Example usage:
     * <code>
     * $query->filterByBedroom1(1234); // WHERE bedroom1 = 1234
     * $query->filterByBedroom1(array(12, 34)); // WHERE bedroom1 IN (12, 34)
     * $query->filterByBedroom1(array('min' => 12)); // WHERE bedroom1 > 12
     * </code>
     *
     * @see       filterByBedroomOne()
     *
     * @param     mixed $bedroom1 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitQuery The current query, for fluid interface
     */
    public function filterByBedroom1($bedroom1 = null, $comparison = null)
    {
        if (is_array($bedroom1)) {
            $useMinMax = false;
            if (isset($bedroom1['min'])) {
                $this->addUsingAlias(UnitTableMap::COL_BEDROOM1, $bedroom1['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bedroom1['max'])) {
                $this->addUsingAlias(UnitTableMap::COL_BEDROOM1, $bedroom1['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UnitTableMap::COL_BEDROOM1, $bedroom1, $comparison);
    }

    /**
     * Filter the query on the bedroom2 column
     *
     * Example usage:
     * <code>
     * $query->filterByBedroom2(1234); // WHERE bedroom2 = 1234
     * $query->filterByBedroom2(array(12, 34)); // WHERE bedroom2 IN (12, 34)
     * $query->filterByBedroom2(array('min' => 12)); // WHERE bedroom2 > 12
     * </code>
     *
     * @see       filterByBedroomTwo()
     *
     * @param     mixed $bedroom2 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitQuery The current query, for fluid interface
     */
    public function filterByBedroom2($bedroom2 = null, $comparison = null)
    {
        if (is_array($bedroom2)) {
            $useMinMax = false;
            if (isset($bedroom2['min'])) {
                $this->addUsingAlias(UnitTableMap::COL_BEDROOM2, $bedroom2['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bedroom2['max'])) {
                $this->addUsingAlias(UnitTableMap::COL_BEDROOM2, $bedroom2['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UnitTableMap::COL_BEDROOM2, $bedroom2, $comparison);
    }

    /**
     * Filter the query on the bedroom3 column
     *
     * Example usage:
     * <code>
     * $query->filterByBedroom3(1234); // WHERE bedroom3 = 1234
     * $query->filterByBedroom3(array(12, 34)); // WHERE bedroom3 IN (12, 34)
     * $query->filterByBedroom3(array('min' => 12)); // WHERE bedroom3 > 12
     * </code>
     *
     * @see       filterByBedroomThree()
     *
     * @param     mixed $bedroom3 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitQuery The current query, for fluid interface
     */
    public function filterByBedroom3($bedroom3 = null, $comparison = null)
    {
        if (is_array($bedroom3)) {
            $useMinMax = false;
            if (isset($bedroom3['min'])) {
                $this->addUsingAlias(UnitTableMap::COL_BEDROOM3, $bedroom3['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bedroom3['max'])) {
                $this->addUsingAlias(UnitTableMap::COL_BEDROOM3, $bedroom3['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UnitTableMap::COL_BEDROOM3, $bedroom3, $comparison);
    }

    /**
     * Filter the query on the bedroom4 column
     *
     * Example usage:
     * <code>
     * $query->filterByBedroom4(1234); // WHERE bedroom4 = 1234
     * $query->filterByBedroom4(array(12, 34)); // WHERE bedroom4 IN (12, 34)
     * $query->filterByBedroom4(array('min' => 12)); // WHERE bedroom4 > 12
     * </code>
     *
     * @see       filterByBedroomFour()
     *
     * @param     mixed $bedroom4 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitQuery The current query, for fluid interface
     */
    public function filterByBedroom4($bedroom4 = null, $comparison = null)
    {
        if (is_array($bedroom4)) {
            $useMinMax = false;
            if (isset($bedroom4['min'])) {
                $this->addUsingAlias(UnitTableMap::COL_BEDROOM4, $bedroom4['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bedroom4['max'])) {
                $this->addUsingAlias(UnitTableMap::COL_BEDROOM4, $bedroom4['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UnitTableMap::COL_BEDROOM4, $bedroom4, $comparison);
    }

    /**
     * Filter the query by a related \Bedroom object
     *
     * @param \Bedroom|ObjectCollection $bedroom The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUnitQuery The current query, for fluid interface
     */
    public function filterByBedroomOne($bedroom, $comparison = null)
    {
        if ($bedroom instanceof \Bedroom) {
            return $this
                ->addUsingAlias(UnitTableMap::COL_BEDROOM1, $bedroom->getId(), $comparison);
        } elseif ($bedroom instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UnitTableMap::COL_BEDROOM1, $bedroom->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByBedroomOne() only accepts arguments of type \Bedroom or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BedroomOne relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUnitQuery The current query, for fluid interface
     */
    public function joinBedroomOne($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BedroomOne');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BedroomOne');
        }

        return $this;
    }

    /**
     * Use the BedroomOne relation Bedroom object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BedroomQuery A secondary query class using the current class as primary query
     */
    public function useBedroomOneQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBedroomOne($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BedroomOne', '\BedroomQuery');
    }

    /**
     * Filter the query by a related \Bedroom object
     *
     * @param \Bedroom|ObjectCollection $bedroom The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUnitQuery The current query, for fluid interface
     */
    public function filterByBedroomTwo($bedroom, $comparison = null)
    {
        if ($bedroom instanceof \Bedroom) {
            return $this
                ->addUsingAlias(UnitTableMap::COL_BEDROOM2, $bedroom->getId(), $comparison);
        } elseif ($bedroom instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UnitTableMap::COL_BEDROOM2, $bedroom->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByBedroomTwo() only accepts arguments of type \Bedroom or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BedroomTwo relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUnitQuery The current query, for fluid interface
     */
    public function joinBedroomTwo($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BedroomTwo');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BedroomTwo');
        }

        return $this;
    }

    /**
     * Use the BedroomTwo relation Bedroom object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BedroomQuery A secondary query class using the current class as primary query
     */
    public function useBedroomTwoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBedroomTwo($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BedroomTwo', '\BedroomQuery');
    }

    /**
     * Filter the query by a related \Bedroom object
     *
     * @param \Bedroom|ObjectCollection $bedroom The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUnitQuery The current query, for fluid interface
     */
    public function filterByBedroomThree($bedroom, $comparison = null)
    {
        if ($bedroom instanceof \Bedroom) {
            return $this
                ->addUsingAlias(UnitTableMap::COL_BEDROOM3, $bedroom->getId(), $comparison);
        } elseif ($bedroom instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UnitTableMap::COL_BEDROOM3, $bedroom->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByBedroomThree() only accepts arguments of type \Bedroom or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BedroomThree relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUnitQuery The current query, for fluid interface
     */
    public function joinBedroomThree($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BedroomThree');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BedroomThree');
        }

        return $this;
    }

    /**
     * Use the BedroomThree relation Bedroom object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BedroomQuery A secondary query class using the current class as primary query
     */
    public function useBedroomThreeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBedroomThree($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BedroomThree', '\BedroomQuery');
    }

    /**
     * Filter the query by a related \Bedroom object
     *
     * @param \Bedroom|ObjectCollection $bedroom The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUnitQuery The current query, for fluid interface
     */
    public function filterByBedroomFour($bedroom, $comparison = null)
    {
        if ($bedroom instanceof \Bedroom) {
            return $this
                ->addUsingAlias(UnitTableMap::COL_BEDROOM4, $bedroom->getId(), $comparison);
        } elseif ($bedroom instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UnitTableMap::COL_BEDROOM4, $bedroom->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByBedroomFour() only accepts arguments of type \Bedroom or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BedroomFour relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUnitQuery The current query, for fluid interface
     */
    public function joinBedroomFour($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BedroomFour');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BedroomFour');
        }

        return $this;
    }

    /**
     * Use the BedroomFour relation Bedroom object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BedroomQuery A secondary query class using the current class as primary query
     */
    public function useBedroomFourQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBedroomFour($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BedroomFour', '\BedroomQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUnit $unit Object to remove from the list of results
     *
     * @return $this|ChildUnitQuery The current query, for fluid interface
     */
    public function prune($unit = null)
    {
        if ($unit) {
            $this->addUsingAlias(UnitTableMap::COL_ID, $unit->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the unit table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UnitTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UnitTableMap::clearInstancePool();
            UnitTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UnitTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UnitTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UnitTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UnitTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UnitQuery
