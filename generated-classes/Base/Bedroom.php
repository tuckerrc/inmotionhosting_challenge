<?php

namespace Base;

use \Bedroom as ChildBedroom;
use \BedroomQuery as ChildBedroomQuery;
use \Student as ChildStudent;
use \StudentQuery as ChildStudentQuery;
use \Unit as ChildUnit;
use \UnitQuery as ChildUnitQuery;
use \Exception;
use \PDO;
use Map\BedroomTableMap;
use Map\UnitTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'bedroom' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Bedroom implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\BedroomTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the occupant1 field.
     *
     * @var        int
     */
    protected $occupant1;

    /**
     * The value for the occupant2 field.
     *
     * @var        int
     */
    protected $occupant2;

    /**
     * @var        ChildStudent
     */
    protected $aStudent;

    /**
     * @var        ChildStudent
     */
    protected $aStudent2;

    /**
     * @var        ObjectCollection|ChildUnit[] Collection to store aggregation of ChildUnit objects.
     */
    protected $collBedroomOnes;
    protected $collBedroomOnesPartial;

    /**
     * @var        ObjectCollection|ChildUnit[] Collection to store aggregation of ChildUnit objects.
     */
    protected $collBedroomTwos;
    protected $collBedroomTwosPartial;

    /**
     * @var        ObjectCollection|ChildUnit[] Collection to store aggregation of ChildUnit objects.
     */
    protected $collBedroomThrees;
    protected $collBedroomThreesPartial;

    /**
     * @var        ObjectCollection|ChildUnit[] Collection to store aggregation of ChildUnit objects.
     */
    protected $collBedroomFours;
    protected $collBedroomFoursPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUnit[]
     */
    protected $bedroomOnesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUnit[]
     */
    protected $bedroomTwosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUnit[]
     */
    protected $bedroomThreesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUnit[]
     */
    protected $bedroomFoursScheduledForDeletion = null;

    /**
     * Initializes internal state of Base\Bedroom object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Bedroom</code> instance.  If
     * <code>obj</code> is an instance of <code>Bedroom</code>, delegates to
     * <code>equals(Bedroom)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Bedroom The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [occupant1] column value.
     *
     * @return int
     */
    public function getOccupant1()
    {
        return $this->occupant1;
    }

    /**
     * Get the [occupant2] column value.
     *
     * @return int
     */
    public function getOccupant2()
    {
        return $this->occupant2;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Bedroom The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[BedroomTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [occupant1] column.
     *
     * @param int $v new value
     * @return $this|\Bedroom The current object (for fluent API support)
     */
    public function setOccupant1($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->occupant1 !== $v) {
            $this->occupant1 = $v;
            $this->modifiedColumns[BedroomTableMap::COL_OCCUPANT1] = true;
        }

        if ($this->aStudent !== null && $this->aStudent->getId() !== $v) {
            $this->aStudent = null;
        }

        return $this;
    } // setOccupant1()

    /**
     * Set the value of [occupant2] column.
     *
     * @param int $v new value
     * @return $this|\Bedroom The current object (for fluent API support)
     */
    public function setOccupant2($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->occupant2 !== $v) {
            $this->occupant2 = $v;
            $this->modifiedColumns[BedroomTableMap::COL_OCCUPANT2] = true;
        }

        if ($this->aStudent2 !== null && $this->aStudent2->getId() !== $v) {
            $this->aStudent2 = null;
        }

        return $this;
    } // setOccupant2()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : BedroomTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : BedroomTableMap::translateFieldName('Occupant1', TableMap::TYPE_PHPNAME, $indexType)];
            $this->occupant1 = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : BedroomTableMap::translateFieldName('Occupant2', TableMap::TYPE_PHPNAME, $indexType)];
            $this->occupant2 = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 3; // 3 = BedroomTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Bedroom'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aStudent !== null && $this->occupant1 !== $this->aStudent->getId()) {
            $this->aStudent = null;
        }
        if ($this->aStudent2 !== null && $this->occupant2 !== $this->aStudent2->getId()) {
            $this->aStudent2 = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BedroomTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildBedroomQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aStudent = null;
            $this->aStudent2 = null;
            $this->collBedroomOnes = null;

            $this->collBedroomTwos = null;

            $this->collBedroomThrees = null;

            $this->collBedroomFours = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Bedroom::setDeleted()
     * @see Bedroom::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(BedroomTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildBedroomQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(BedroomTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                BedroomTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aStudent !== null) {
                if ($this->aStudent->isModified() || $this->aStudent->isNew()) {
                    $affectedRows += $this->aStudent->save($con);
                }
                $this->setStudent($this->aStudent);
            }

            if ($this->aStudent2 !== null) {
                if ($this->aStudent2->isModified() || $this->aStudent2->isNew()) {
                    $affectedRows += $this->aStudent2->save($con);
                }
                $this->setStudent2($this->aStudent2);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->bedroomOnesScheduledForDeletion !== null) {
                if (!$this->bedroomOnesScheduledForDeletion->isEmpty()) {
                    foreach ($this->bedroomOnesScheduledForDeletion as $bedroomOne) {
                        // need to save related object because we set the relation to null
                        $bedroomOne->save($con);
                    }
                    $this->bedroomOnesScheduledForDeletion = null;
                }
            }

            if ($this->collBedroomOnes !== null) {
                foreach ($this->collBedroomOnes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->bedroomTwosScheduledForDeletion !== null) {
                if (!$this->bedroomTwosScheduledForDeletion->isEmpty()) {
                    foreach ($this->bedroomTwosScheduledForDeletion as $bedroomTwo) {
                        // need to save related object because we set the relation to null
                        $bedroomTwo->save($con);
                    }
                    $this->bedroomTwosScheduledForDeletion = null;
                }
            }

            if ($this->collBedroomTwos !== null) {
                foreach ($this->collBedroomTwos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->bedroomThreesScheduledForDeletion !== null) {
                if (!$this->bedroomThreesScheduledForDeletion->isEmpty()) {
                    foreach ($this->bedroomThreesScheduledForDeletion as $bedroomThree) {
                        // need to save related object because we set the relation to null
                        $bedroomThree->save($con);
                    }
                    $this->bedroomThreesScheduledForDeletion = null;
                }
            }

            if ($this->collBedroomThrees !== null) {
                foreach ($this->collBedroomThrees as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->bedroomFoursScheduledForDeletion !== null) {
                if (!$this->bedroomFoursScheduledForDeletion->isEmpty()) {
                    foreach ($this->bedroomFoursScheduledForDeletion as $bedroomFour) {
                        // need to save related object because we set the relation to null
                        $bedroomFour->save($con);
                    }
                    $this->bedroomFoursScheduledForDeletion = null;
                }
            }

            if ($this->collBedroomFours !== null) {
                foreach ($this->collBedroomFours as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[BedroomTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . BedroomTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(BedroomTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(BedroomTableMap::COL_OCCUPANT1)) {
            $modifiedColumns[':p' . $index++]  = 'occupant1';
        }
        if ($this->isColumnModified(BedroomTableMap::COL_OCCUPANT2)) {
            $modifiedColumns[':p' . $index++]  = 'occupant2';
        }

        $sql = sprintf(
            'INSERT INTO bedroom (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'occupant1':
                        $stmt->bindValue($identifier, $this->occupant1, PDO::PARAM_INT);
                        break;
                    case 'occupant2':
                        $stmt->bindValue($identifier, $this->occupant2, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = BedroomTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getOccupant1();
                break;
            case 2:
                return $this->getOccupant2();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Bedroom'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Bedroom'][$this->hashCode()] = true;
        $keys = BedroomTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getOccupant1(),
            $keys[2] => $this->getOccupant2(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aStudent) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'student';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'student';
                        break;
                    default:
                        $key = 'Student';
                }

                $result[$key] = $this->aStudent->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aStudent2) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'student';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'student';
                        break;
                    default:
                        $key = 'Student2';
                }

                $result[$key] = $this->aStudent2->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collBedroomOnes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'units';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'units';
                        break;
                    default:
                        $key = 'BedroomOnes';
                }

                $result[$key] = $this->collBedroomOnes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBedroomTwos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'units';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'units';
                        break;
                    default:
                        $key = 'BedroomTwos';
                }

                $result[$key] = $this->collBedroomTwos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBedroomThrees) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'units';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'units';
                        break;
                    default:
                        $key = 'BedroomThrees';
                }

                $result[$key] = $this->collBedroomThrees->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBedroomFours) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'units';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'units';
                        break;
                    default:
                        $key = 'BedroomFours';
                }

                $result[$key] = $this->collBedroomFours->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Bedroom
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = BedroomTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Bedroom
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setOccupant1($value);
                break;
            case 2:
                $this->setOccupant2($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = BedroomTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setOccupant1($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setOccupant2($arr[$keys[2]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Bedroom The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(BedroomTableMap::DATABASE_NAME);

        if ($this->isColumnModified(BedroomTableMap::COL_ID)) {
            $criteria->add(BedroomTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(BedroomTableMap::COL_OCCUPANT1)) {
            $criteria->add(BedroomTableMap::COL_OCCUPANT1, $this->occupant1);
        }
        if ($this->isColumnModified(BedroomTableMap::COL_OCCUPANT2)) {
            $criteria->add(BedroomTableMap::COL_OCCUPANT2, $this->occupant2);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildBedroomQuery::create();
        $criteria->add(BedroomTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Bedroom (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setOccupant1($this->getOccupant1());
        $copyObj->setOccupant2($this->getOccupant2());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getBedroomOnes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBedroomOne($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBedroomTwos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBedroomTwo($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBedroomThrees() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBedroomThree($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBedroomFours() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBedroomFour($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Bedroom Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildStudent object.
     *
     * @param  ChildStudent $v
     * @return $this|\Bedroom The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStudent(ChildStudent $v = null)
    {
        if ($v === null) {
            $this->setOccupant1(NULL);
        } else {
            $this->setOccupant1($v->getId());
        }

        $this->aStudent = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildStudent object, it will not be re-added.
        if ($v !== null) {
            $v->addStudent($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildStudent object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildStudent The associated ChildStudent object.
     * @throws PropelException
     */
    public function getStudent(ConnectionInterface $con = null)
    {
        if ($this->aStudent === null && ($this->occupant1 !== null)) {
            $this->aStudent = ChildStudentQuery::create()->findPk($this->occupant1, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aStudent->addStudents($this);
             */
        }

        return $this->aStudent;
    }

    /**
     * Declares an association between this object and a ChildStudent object.
     *
     * @param  ChildStudent $v
     * @return $this|\Bedroom The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStudent2(ChildStudent $v = null)
    {
        if ($v === null) {
            $this->setOccupant2(NULL);
        } else {
            $this->setOccupant2($v->getId());
        }

        $this->aStudent2 = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildStudent object, it will not be re-added.
        if ($v !== null) {
            $v->addStudent2($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildStudent object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildStudent The associated ChildStudent object.
     * @throws PropelException
     */
    public function getStudent2(ConnectionInterface $con = null)
    {
        if ($this->aStudent2 === null && ($this->occupant2 !== null)) {
            $this->aStudent2 = ChildStudentQuery::create()->findPk($this->occupant2, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aStudent2->addStudent2s($this);
             */
        }

        return $this->aStudent2;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('BedroomOne' == $relationName) {
            $this->initBedroomOnes();
            return;
        }
        if ('BedroomTwo' == $relationName) {
            $this->initBedroomTwos();
            return;
        }
        if ('BedroomThree' == $relationName) {
            $this->initBedroomThrees();
            return;
        }
        if ('BedroomFour' == $relationName) {
            $this->initBedroomFours();
            return;
        }
    }

    /**
     * Clears out the collBedroomOnes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBedroomOnes()
     */
    public function clearBedroomOnes()
    {
        $this->collBedroomOnes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBedroomOnes collection loaded partially.
     */
    public function resetPartialBedroomOnes($v = true)
    {
        $this->collBedroomOnesPartial = $v;
    }

    /**
     * Initializes the collBedroomOnes collection.
     *
     * By default this just sets the collBedroomOnes collection to an empty array (like clearcollBedroomOnes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBedroomOnes($overrideExisting = true)
    {
        if (null !== $this->collBedroomOnes && !$overrideExisting) {
            return;
        }

        $collectionClassName = UnitTableMap::getTableMap()->getCollectionClassName();

        $this->collBedroomOnes = new $collectionClassName;
        $this->collBedroomOnes->setModel('\Unit');
    }

    /**
     * Gets an array of ChildUnit objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildBedroom is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUnit[] List of ChildUnit objects
     * @throws PropelException
     */
    public function getBedroomOnes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBedroomOnesPartial && !$this->isNew();
        if (null === $this->collBedroomOnes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBedroomOnes) {
                // return empty collection
                $this->initBedroomOnes();
            } else {
                $collBedroomOnes = ChildUnitQuery::create(null, $criteria)
                    ->filterByBedroomOne($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBedroomOnesPartial && count($collBedroomOnes)) {
                        $this->initBedroomOnes(false);

                        foreach ($collBedroomOnes as $obj) {
                            if (false == $this->collBedroomOnes->contains($obj)) {
                                $this->collBedroomOnes->append($obj);
                            }
                        }

                        $this->collBedroomOnesPartial = true;
                    }

                    return $collBedroomOnes;
                }

                if ($partial && $this->collBedroomOnes) {
                    foreach ($this->collBedroomOnes as $obj) {
                        if ($obj->isNew()) {
                            $collBedroomOnes[] = $obj;
                        }
                    }
                }

                $this->collBedroomOnes = $collBedroomOnes;
                $this->collBedroomOnesPartial = false;
            }
        }

        return $this->collBedroomOnes;
    }

    /**
     * Sets a collection of ChildUnit objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $bedroomOnes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildBedroom The current object (for fluent API support)
     */
    public function setBedroomOnes(Collection $bedroomOnes, ConnectionInterface $con = null)
    {
        /** @var ChildUnit[] $bedroomOnesToDelete */
        $bedroomOnesToDelete = $this->getBedroomOnes(new Criteria(), $con)->diff($bedroomOnes);


        $this->bedroomOnesScheduledForDeletion = $bedroomOnesToDelete;

        foreach ($bedroomOnesToDelete as $bedroomOneRemoved) {
            $bedroomOneRemoved->setBedroomOne(null);
        }

        $this->collBedroomOnes = null;
        foreach ($bedroomOnes as $bedroomOne) {
            $this->addBedroomOne($bedroomOne);
        }

        $this->collBedroomOnes = $bedroomOnes;
        $this->collBedroomOnesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Unit objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Unit objects.
     * @throws PropelException
     */
    public function countBedroomOnes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBedroomOnesPartial && !$this->isNew();
        if (null === $this->collBedroomOnes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBedroomOnes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBedroomOnes());
            }

            $query = ChildUnitQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBedroomOne($this)
                ->count($con);
        }

        return count($this->collBedroomOnes);
    }

    /**
     * Method called to associate a ChildUnit object to this object
     * through the ChildUnit foreign key attribute.
     *
     * @param  ChildUnit $l ChildUnit
     * @return $this|\Bedroom The current object (for fluent API support)
     */
    public function addBedroomOne(ChildUnit $l)
    {
        if ($this->collBedroomOnes === null) {
            $this->initBedroomOnes();
            $this->collBedroomOnesPartial = true;
        }

        if (!$this->collBedroomOnes->contains($l)) {
            $this->doAddBedroomOne($l);

            if ($this->bedroomOnesScheduledForDeletion and $this->bedroomOnesScheduledForDeletion->contains($l)) {
                $this->bedroomOnesScheduledForDeletion->remove($this->bedroomOnesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUnit $bedroomOne The ChildUnit object to add.
     */
    protected function doAddBedroomOne(ChildUnit $bedroomOne)
    {
        $this->collBedroomOnes[]= $bedroomOne;
        $bedroomOne->setBedroomOne($this);
    }

    /**
     * @param  ChildUnit $bedroomOne The ChildUnit object to remove.
     * @return $this|ChildBedroom The current object (for fluent API support)
     */
    public function removeBedroomOne(ChildUnit $bedroomOne)
    {
        if ($this->getBedroomOnes()->contains($bedroomOne)) {
            $pos = $this->collBedroomOnes->search($bedroomOne);
            $this->collBedroomOnes->remove($pos);
            if (null === $this->bedroomOnesScheduledForDeletion) {
                $this->bedroomOnesScheduledForDeletion = clone $this->collBedroomOnes;
                $this->bedroomOnesScheduledForDeletion->clear();
            }
            $this->bedroomOnesScheduledForDeletion[]= $bedroomOne;
            $bedroomOne->setBedroomOne(null);
        }

        return $this;
    }

    /**
     * Clears out the collBedroomTwos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBedroomTwos()
     */
    public function clearBedroomTwos()
    {
        $this->collBedroomTwos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBedroomTwos collection loaded partially.
     */
    public function resetPartialBedroomTwos($v = true)
    {
        $this->collBedroomTwosPartial = $v;
    }

    /**
     * Initializes the collBedroomTwos collection.
     *
     * By default this just sets the collBedroomTwos collection to an empty array (like clearcollBedroomTwos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBedroomTwos($overrideExisting = true)
    {
        if (null !== $this->collBedroomTwos && !$overrideExisting) {
            return;
        }

        $collectionClassName = UnitTableMap::getTableMap()->getCollectionClassName();

        $this->collBedroomTwos = new $collectionClassName;
        $this->collBedroomTwos->setModel('\Unit');
    }

    /**
     * Gets an array of ChildUnit objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildBedroom is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUnit[] List of ChildUnit objects
     * @throws PropelException
     */
    public function getBedroomTwos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBedroomTwosPartial && !$this->isNew();
        if (null === $this->collBedroomTwos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBedroomTwos) {
                // return empty collection
                $this->initBedroomTwos();
            } else {
                $collBedroomTwos = ChildUnitQuery::create(null, $criteria)
                    ->filterByBedroomTwo($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBedroomTwosPartial && count($collBedroomTwos)) {
                        $this->initBedroomTwos(false);

                        foreach ($collBedroomTwos as $obj) {
                            if (false == $this->collBedroomTwos->contains($obj)) {
                                $this->collBedroomTwos->append($obj);
                            }
                        }

                        $this->collBedroomTwosPartial = true;
                    }

                    return $collBedroomTwos;
                }

                if ($partial && $this->collBedroomTwos) {
                    foreach ($this->collBedroomTwos as $obj) {
                        if ($obj->isNew()) {
                            $collBedroomTwos[] = $obj;
                        }
                    }
                }

                $this->collBedroomTwos = $collBedroomTwos;
                $this->collBedroomTwosPartial = false;
            }
        }

        return $this->collBedroomTwos;
    }

    /**
     * Sets a collection of ChildUnit objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $bedroomTwos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildBedroom The current object (for fluent API support)
     */
    public function setBedroomTwos(Collection $bedroomTwos, ConnectionInterface $con = null)
    {
        /** @var ChildUnit[] $bedroomTwosToDelete */
        $bedroomTwosToDelete = $this->getBedroomTwos(new Criteria(), $con)->diff($bedroomTwos);


        $this->bedroomTwosScheduledForDeletion = $bedroomTwosToDelete;

        foreach ($bedroomTwosToDelete as $bedroomTwoRemoved) {
            $bedroomTwoRemoved->setBedroomTwo(null);
        }

        $this->collBedroomTwos = null;
        foreach ($bedroomTwos as $bedroomTwo) {
            $this->addBedroomTwo($bedroomTwo);
        }

        $this->collBedroomTwos = $bedroomTwos;
        $this->collBedroomTwosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Unit objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Unit objects.
     * @throws PropelException
     */
    public function countBedroomTwos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBedroomTwosPartial && !$this->isNew();
        if (null === $this->collBedroomTwos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBedroomTwos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBedroomTwos());
            }

            $query = ChildUnitQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBedroomTwo($this)
                ->count($con);
        }

        return count($this->collBedroomTwos);
    }

    /**
     * Method called to associate a ChildUnit object to this object
     * through the ChildUnit foreign key attribute.
     *
     * @param  ChildUnit $l ChildUnit
     * @return $this|\Bedroom The current object (for fluent API support)
     */
    public function addBedroomTwo(ChildUnit $l)
    {
        if ($this->collBedroomTwos === null) {
            $this->initBedroomTwos();
            $this->collBedroomTwosPartial = true;
        }

        if (!$this->collBedroomTwos->contains($l)) {
            $this->doAddBedroomTwo($l);

            if ($this->bedroomTwosScheduledForDeletion and $this->bedroomTwosScheduledForDeletion->contains($l)) {
                $this->bedroomTwosScheduledForDeletion->remove($this->bedroomTwosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUnit $bedroomTwo The ChildUnit object to add.
     */
    protected function doAddBedroomTwo(ChildUnit $bedroomTwo)
    {
        $this->collBedroomTwos[]= $bedroomTwo;
        $bedroomTwo->setBedroomTwo($this);
    }

    /**
     * @param  ChildUnit $bedroomTwo The ChildUnit object to remove.
     * @return $this|ChildBedroom The current object (for fluent API support)
     */
    public function removeBedroomTwo(ChildUnit $bedroomTwo)
    {
        if ($this->getBedroomTwos()->contains($bedroomTwo)) {
            $pos = $this->collBedroomTwos->search($bedroomTwo);
            $this->collBedroomTwos->remove($pos);
            if (null === $this->bedroomTwosScheduledForDeletion) {
                $this->bedroomTwosScheduledForDeletion = clone $this->collBedroomTwos;
                $this->bedroomTwosScheduledForDeletion->clear();
            }
            $this->bedroomTwosScheduledForDeletion[]= $bedroomTwo;
            $bedroomTwo->setBedroomTwo(null);
        }

        return $this;
    }

    /**
     * Clears out the collBedroomThrees collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBedroomThrees()
     */
    public function clearBedroomThrees()
    {
        $this->collBedroomThrees = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBedroomThrees collection loaded partially.
     */
    public function resetPartialBedroomThrees($v = true)
    {
        $this->collBedroomThreesPartial = $v;
    }

    /**
     * Initializes the collBedroomThrees collection.
     *
     * By default this just sets the collBedroomThrees collection to an empty array (like clearcollBedroomThrees());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBedroomThrees($overrideExisting = true)
    {
        if (null !== $this->collBedroomThrees && !$overrideExisting) {
            return;
        }

        $collectionClassName = UnitTableMap::getTableMap()->getCollectionClassName();

        $this->collBedroomThrees = new $collectionClassName;
        $this->collBedroomThrees->setModel('\Unit');
    }

    /**
     * Gets an array of ChildUnit objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildBedroom is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUnit[] List of ChildUnit objects
     * @throws PropelException
     */
    public function getBedroomThrees(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBedroomThreesPartial && !$this->isNew();
        if (null === $this->collBedroomThrees || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBedroomThrees) {
                // return empty collection
                $this->initBedroomThrees();
            } else {
                $collBedroomThrees = ChildUnitQuery::create(null, $criteria)
                    ->filterByBedroomThree($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBedroomThreesPartial && count($collBedroomThrees)) {
                        $this->initBedroomThrees(false);

                        foreach ($collBedroomThrees as $obj) {
                            if (false == $this->collBedroomThrees->contains($obj)) {
                                $this->collBedroomThrees->append($obj);
                            }
                        }

                        $this->collBedroomThreesPartial = true;
                    }

                    return $collBedroomThrees;
                }

                if ($partial && $this->collBedroomThrees) {
                    foreach ($this->collBedroomThrees as $obj) {
                        if ($obj->isNew()) {
                            $collBedroomThrees[] = $obj;
                        }
                    }
                }

                $this->collBedroomThrees = $collBedroomThrees;
                $this->collBedroomThreesPartial = false;
            }
        }

        return $this->collBedroomThrees;
    }

    /**
     * Sets a collection of ChildUnit objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $bedroomThrees A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildBedroom The current object (for fluent API support)
     */
    public function setBedroomThrees(Collection $bedroomThrees, ConnectionInterface $con = null)
    {
        /** @var ChildUnit[] $bedroomThreesToDelete */
        $bedroomThreesToDelete = $this->getBedroomThrees(new Criteria(), $con)->diff($bedroomThrees);


        $this->bedroomThreesScheduledForDeletion = $bedroomThreesToDelete;

        foreach ($bedroomThreesToDelete as $bedroomThreeRemoved) {
            $bedroomThreeRemoved->setBedroomThree(null);
        }

        $this->collBedroomThrees = null;
        foreach ($bedroomThrees as $bedroomThree) {
            $this->addBedroomThree($bedroomThree);
        }

        $this->collBedroomThrees = $bedroomThrees;
        $this->collBedroomThreesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Unit objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Unit objects.
     * @throws PropelException
     */
    public function countBedroomThrees(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBedroomThreesPartial && !$this->isNew();
        if (null === $this->collBedroomThrees || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBedroomThrees) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBedroomThrees());
            }

            $query = ChildUnitQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBedroomThree($this)
                ->count($con);
        }

        return count($this->collBedroomThrees);
    }

    /**
     * Method called to associate a ChildUnit object to this object
     * through the ChildUnit foreign key attribute.
     *
     * @param  ChildUnit $l ChildUnit
     * @return $this|\Bedroom The current object (for fluent API support)
     */
    public function addBedroomThree(ChildUnit $l)
    {
        if ($this->collBedroomThrees === null) {
            $this->initBedroomThrees();
            $this->collBedroomThreesPartial = true;
        }

        if (!$this->collBedroomThrees->contains($l)) {
            $this->doAddBedroomThree($l);

            if ($this->bedroomThreesScheduledForDeletion and $this->bedroomThreesScheduledForDeletion->contains($l)) {
                $this->bedroomThreesScheduledForDeletion->remove($this->bedroomThreesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUnit $bedroomThree The ChildUnit object to add.
     */
    protected function doAddBedroomThree(ChildUnit $bedroomThree)
    {
        $this->collBedroomThrees[]= $bedroomThree;
        $bedroomThree->setBedroomThree($this);
    }

    /**
     * @param  ChildUnit $bedroomThree The ChildUnit object to remove.
     * @return $this|ChildBedroom The current object (for fluent API support)
     */
    public function removeBedroomThree(ChildUnit $bedroomThree)
    {
        if ($this->getBedroomThrees()->contains($bedroomThree)) {
            $pos = $this->collBedroomThrees->search($bedroomThree);
            $this->collBedroomThrees->remove($pos);
            if (null === $this->bedroomThreesScheduledForDeletion) {
                $this->bedroomThreesScheduledForDeletion = clone $this->collBedroomThrees;
                $this->bedroomThreesScheduledForDeletion->clear();
            }
            $this->bedroomThreesScheduledForDeletion[]= $bedroomThree;
            $bedroomThree->setBedroomThree(null);
        }

        return $this;
    }

    /**
     * Clears out the collBedroomFours collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBedroomFours()
     */
    public function clearBedroomFours()
    {
        $this->collBedroomFours = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBedroomFours collection loaded partially.
     */
    public function resetPartialBedroomFours($v = true)
    {
        $this->collBedroomFoursPartial = $v;
    }

    /**
     * Initializes the collBedroomFours collection.
     *
     * By default this just sets the collBedroomFours collection to an empty array (like clearcollBedroomFours());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBedroomFours($overrideExisting = true)
    {
        if (null !== $this->collBedroomFours && !$overrideExisting) {
            return;
        }

        $collectionClassName = UnitTableMap::getTableMap()->getCollectionClassName();

        $this->collBedroomFours = new $collectionClassName;
        $this->collBedroomFours->setModel('\Unit');
    }

    /**
     * Gets an array of ChildUnit objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildBedroom is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUnit[] List of ChildUnit objects
     * @throws PropelException
     */
    public function getBedroomFours(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBedroomFoursPartial && !$this->isNew();
        if (null === $this->collBedroomFours || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBedroomFours) {
                // return empty collection
                $this->initBedroomFours();
            } else {
                $collBedroomFours = ChildUnitQuery::create(null, $criteria)
                    ->filterByBedroomFour($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBedroomFoursPartial && count($collBedroomFours)) {
                        $this->initBedroomFours(false);

                        foreach ($collBedroomFours as $obj) {
                            if (false == $this->collBedroomFours->contains($obj)) {
                                $this->collBedroomFours->append($obj);
                            }
                        }

                        $this->collBedroomFoursPartial = true;
                    }

                    return $collBedroomFours;
                }

                if ($partial && $this->collBedroomFours) {
                    foreach ($this->collBedroomFours as $obj) {
                        if ($obj->isNew()) {
                            $collBedroomFours[] = $obj;
                        }
                    }
                }

                $this->collBedroomFours = $collBedroomFours;
                $this->collBedroomFoursPartial = false;
            }
        }

        return $this->collBedroomFours;
    }

    /**
     * Sets a collection of ChildUnit objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $bedroomFours A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildBedroom The current object (for fluent API support)
     */
    public function setBedroomFours(Collection $bedroomFours, ConnectionInterface $con = null)
    {
        /** @var ChildUnit[] $bedroomFoursToDelete */
        $bedroomFoursToDelete = $this->getBedroomFours(new Criteria(), $con)->diff($bedroomFours);


        $this->bedroomFoursScheduledForDeletion = $bedroomFoursToDelete;

        foreach ($bedroomFoursToDelete as $bedroomFourRemoved) {
            $bedroomFourRemoved->setBedroomFour(null);
        }

        $this->collBedroomFours = null;
        foreach ($bedroomFours as $bedroomFour) {
            $this->addBedroomFour($bedroomFour);
        }

        $this->collBedroomFours = $bedroomFours;
        $this->collBedroomFoursPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Unit objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Unit objects.
     * @throws PropelException
     */
    public function countBedroomFours(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBedroomFoursPartial && !$this->isNew();
        if (null === $this->collBedroomFours || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBedroomFours) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBedroomFours());
            }

            $query = ChildUnitQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBedroomFour($this)
                ->count($con);
        }

        return count($this->collBedroomFours);
    }

    /**
     * Method called to associate a ChildUnit object to this object
     * through the ChildUnit foreign key attribute.
     *
     * @param  ChildUnit $l ChildUnit
     * @return $this|\Bedroom The current object (for fluent API support)
     */
    public function addBedroomFour(ChildUnit $l)
    {
        if ($this->collBedroomFours === null) {
            $this->initBedroomFours();
            $this->collBedroomFoursPartial = true;
        }

        if (!$this->collBedroomFours->contains($l)) {
            $this->doAddBedroomFour($l);

            if ($this->bedroomFoursScheduledForDeletion and $this->bedroomFoursScheduledForDeletion->contains($l)) {
                $this->bedroomFoursScheduledForDeletion->remove($this->bedroomFoursScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUnit $bedroomFour The ChildUnit object to add.
     */
    protected function doAddBedroomFour(ChildUnit $bedroomFour)
    {
        $this->collBedroomFours[]= $bedroomFour;
        $bedroomFour->setBedroomFour($this);
    }

    /**
     * @param  ChildUnit $bedroomFour The ChildUnit object to remove.
     * @return $this|ChildBedroom The current object (for fluent API support)
     */
    public function removeBedroomFour(ChildUnit $bedroomFour)
    {
        if ($this->getBedroomFours()->contains($bedroomFour)) {
            $pos = $this->collBedroomFours->search($bedroomFour);
            $this->collBedroomFours->remove($pos);
            if (null === $this->bedroomFoursScheduledForDeletion) {
                $this->bedroomFoursScheduledForDeletion = clone $this->collBedroomFours;
                $this->bedroomFoursScheduledForDeletion->clear();
            }
            $this->bedroomFoursScheduledForDeletion[]= $bedroomFour;
            $bedroomFour->setBedroomFour(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aStudent) {
            $this->aStudent->removeStudent($this);
        }
        if (null !== $this->aStudent2) {
            $this->aStudent2->removeStudent2($this);
        }
        $this->id = null;
        $this->occupant1 = null;
        $this->occupant2 = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collBedroomOnes) {
                foreach ($this->collBedroomOnes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBedroomTwos) {
                foreach ($this->collBedroomTwos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBedroomThrees) {
                foreach ($this->collBedroomThrees as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBedroomFours) {
                foreach ($this->collBedroomFours as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collBedroomOnes = null;
        $this->collBedroomTwos = null;
        $this->collBedroomThrees = null;
        $this->collBedroomFours = null;
        $this->aStudent = null;
        $this->aStudent2 = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(BedroomTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
