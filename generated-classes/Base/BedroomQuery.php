<?php

namespace Base;

use \Bedroom as ChildBedroom;
use \BedroomQuery as ChildBedroomQuery;
use \Exception;
use \PDO;
use Map\BedroomTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'bedroom' table.
 *
 *
 *
 * @method     ChildBedroomQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildBedroomQuery orderByOccupant1($order = Criteria::ASC) Order by the occupant1 column
 * @method     ChildBedroomQuery orderByOccupant2($order = Criteria::ASC) Order by the occupant2 column
 *
 * @method     ChildBedroomQuery groupById() Group by the id column
 * @method     ChildBedroomQuery groupByOccupant1() Group by the occupant1 column
 * @method     ChildBedroomQuery groupByOccupant2() Group by the occupant2 column
 *
 * @method     ChildBedroomQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBedroomQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBedroomQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBedroomQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBedroomQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBedroomQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBedroomQuery leftJoinStudent($relationAlias = null) Adds a LEFT JOIN clause to the query using the Student relation
 * @method     ChildBedroomQuery rightJoinStudent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Student relation
 * @method     ChildBedroomQuery innerJoinStudent($relationAlias = null) Adds a INNER JOIN clause to the query using the Student relation
 *
 * @method     ChildBedroomQuery joinWithStudent($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Student relation
 *
 * @method     ChildBedroomQuery leftJoinWithStudent() Adds a LEFT JOIN clause and with to the query using the Student relation
 * @method     ChildBedroomQuery rightJoinWithStudent() Adds a RIGHT JOIN clause and with to the query using the Student relation
 * @method     ChildBedroomQuery innerJoinWithStudent() Adds a INNER JOIN clause and with to the query using the Student relation
 *
 * @method     ChildBedroomQuery leftJoinStudent2($relationAlias = null) Adds a LEFT JOIN clause to the query using the Student2 relation
 * @method     ChildBedroomQuery rightJoinStudent2($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Student2 relation
 * @method     ChildBedroomQuery innerJoinStudent2($relationAlias = null) Adds a INNER JOIN clause to the query using the Student2 relation
 *
 * @method     ChildBedroomQuery joinWithStudent2($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Student2 relation
 *
 * @method     ChildBedroomQuery leftJoinWithStudent2() Adds a LEFT JOIN clause and with to the query using the Student2 relation
 * @method     ChildBedroomQuery rightJoinWithStudent2() Adds a RIGHT JOIN clause and with to the query using the Student2 relation
 * @method     ChildBedroomQuery innerJoinWithStudent2() Adds a INNER JOIN clause and with to the query using the Student2 relation
 *
 * @method     ChildBedroomQuery leftJoinBedroomOne($relationAlias = null) Adds a LEFT JOIN clause to the query using the BedroomOne relation
 * @method     ChildBedroomQuery rightJoinBedroomOne($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BedroomOne relation
 * @method     ChildBedroomQuery innerJoinBedroomOne($relationAlias = null) Adds a INNER JOIN clause to the query using the BedroomOne relation
 *
 * @method     ChildBedroomQuery joinWithBedroomOne($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BedroomOne relation
 *
 * @method     ChildBedroomQuery leftJoinWithBedroomOne() Adds a LEFT JOIN clause and with to the query using the BedroomOne relation
 * @method     ChildBedroomQuery rightJoinWithBedroomOne() Adds a RIGHT JOIN clause and with to the query using the BedroomOne relation
 * @method     ChildBedroomQuery innerJoinWithBedroomOne() Adds a INNER JOIN clause and with to the query using the BedroomOne relation
 *
 * @method     ChildBedroomQuery leftJoinBedroomTwo($relationAlias = null) Adds a LEFT JOIN clause to the query using the BedroomTwo relation
 * @method     ChildBedroomQuery rightJoinBedroomTwo($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BedroomTwo relation
 * @method     ChildBedroomQuery innerJoinBedroomTwo($relationAlias = null) Adds a INNER JOIN clause to the query using the BedroomTwo relation
 *
 * @method     ChildBedroomQuery joinWithBedroomTwo($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BedroomTwo relation
 *
 * @method     ChildBedroomQuery leftJoinWithBedroomTwo() Adds a LEFT JOIN clause and with to the query using the BedroomTwo relation
 * @method     ChildBedroomQuery rightJoinWithBedroomTwo() Adds a RIGHT JOIN clause and with to the query using the BedroomTwo relation
 * @method     ChildBedroomQuery innerJoinWithBedroomTwo() Adds a INNER JOIN clause and with to the query using the BedroomTwo relation
 *
 * @method     ChildBedroomQuery leftJoinBedroomThree($relationAlias = null) Adds a LEFT JOIN clause to the query using the BedroomThree relation
 * @method     ChildBedroomQuery rightJoinBedroomThree($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BedroomThree relation
 * @method     ChildBedroomQuery innerJoinBedroomThree($relationAlias = null) Adds a INNER JOIN clause to the query using the BedroomThree relation
 *
 * @method     ChildBedroomQuery joinWithBedroomThree($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BedroomThree relation
 *
 * @method     ChildBedroomQuery leftJoinWithBedroomThree() Adds a LEFT JOIN clause and with to the query using the BedroomThree relation
 * @method     ChildBedroomQuery rightJoinWithBedroomThree() Adds a RIGHT JOIN clause and with to the query using the BedroomThree relation
 * @method     ChildBedroomQuery innerJoinWithBedroomThree() Adds a INNER JOIN clause and with to the query using the BedroomThree relation
 *
 * @method     ChildBedroomQuery leftJoinBedroomFour($relationAlias = null) Adds a LEFT JOIN clause to the query using the BedroomFour relation
 * @method     ChildBedroomQuery rightJoinBedroomFour($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BedroomFour relation
 * @method     ChildBedroomQuery innerJoinBedroomFour($relationAlias = null) Adds a INNER JOIN clause to the query using the BedroomFour relation
 *
 * @method     ChildBedroomQuery joinWithBedroomFour($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BedroomFour relation
 *
 * @method     ChildBedroomQuery leftJoinWithBedroomFour() Adds a LEFT JOIN clause and with to the query using the BedroomFour relation
 * @method     ChildBedroomQuery rightJoinWithBedroomFour() Adds a RIGHT JOIN clause and with to the query using the BedroomFour relation
 * @method     ChildBedroomQuery innerJoinWithBedroomFour() Adds a INNER JOIN clause and with to the query using the BedroomFour relation
 *
 * @method     \StudentQuery|\UnitQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildBedroom findOne(ConnectionInterface $con = null) Return the first ChildBedroom matching the query
 * @method     ChildBedroom findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBedroom matching the query, or a new ChildBedroom object populated from the query conditions when no match is found
 *
 * @method     ChildBedroom findOneById(int $id) Return the first ChildBedroom filtered by the id column
 * @method     ChildBedroom findOneByOccupant1(int $occupant1) Return the first ChildBedroom filtered by the occupant1 column
 * @method     ChildBedroom findOneByOccupant2(int $occupant2) Return the first ChildBedroom filtered by the occupant2 column *

 * @method     ChildBedroom requirePk($key, ConnectionInterface $con = null) Return the ChildBedroom by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBedroom requireOne(ConnectionInterface $con = null) Return the first ChildBedroom matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBedroom requireOneById(int $id) Return the first ChildBedroom filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBedroom requireOneByOccupant1(int $occupant1) Return the first ChildBedroom filtered by the occupant1 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBedroom requireOneByOccupant2(int $occupant2) Return the first ChildBedroom filtered by the occupant2 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBedroom[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBedroom objects based on current ModelCriteria
 * @method     ChildBedroom[]|ObjectCollection findById(int $id) Return ChildBedroom objects filtered by the id column
 * @method     ChildBedroom[]|ObjectCollection findByOccupant1(int $occupant1) Return ChildBedroom objects filtered by the occupant1 column
 * @method     ChildBedroom[]|ObjectCollection findByOccupant2(int $occupant2) Return ChildBedroom objects filtered by the occupant2 column
 * @method     ChildBedroom[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BedroomQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\BedroomQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'tucker_chapman', $modelName = '\\Bedroom', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBedroomQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBedroomQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBedroomQuery) {
            return $criteria;
        }
        $query = new ChildBedroomQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBedroom|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BedroomTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BedroomTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBedroom A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, occupant1, occupant2 FROM bedroom WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBedroom $obj */
            $obj = new ChildBedroom();
            $obj->hydrate($row);
            BedroomTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBedroom|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBedroomQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BedroomTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBedroomQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BedroomTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBedroomQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(BedroomTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(BedroomTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BedroomTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the occupant1 column
     *
     * Example usage:
     * <code>
     * $query->filterByOccupant1(1234); // WHERE occupant1 = 1234
     * $query->filterByOccupant1(array(12, 34)); // WHERE occupant1 IN (12, 34)
     * $query->filterByOccupant1(array('min' => 12)); // WHERE occupant1 > 12
     * </code>
     *
     * @see       filterByStudent()
     *
     * @param     mixed $occupant1 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBedroomQuery The current query, for fluid interface
     */
    public function filterByOccupant1($occupant1 = null, $comparison = null)
    {
        if (is_array($occupant1)) {
            $useMinMax = false;
            if (isset($occupant1['min'])) {
                $this->addUsingAlias(BedroomTableMap::COL_OCCUPANT1, $occupant1['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($occupant1['max'])) {
                $this->addUsingAlias(BedroomTableMap::COL_OCCUPANT1, $occupant1['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BedroomTableMap::COL_OCCUPANT1, $occupant1, $comparison);
    }

    /**
     * Filter the query on the occupant2 column
     *
     * Example usage:
     * <code>
     * $query->filterByOccupant2(1234); // WHERE occupant2 = 1234
     * $query->filterByOccupant2(array(12, 34)); // WHERE occupant2 IN (12, 34)
     * $query->filterByOccupant2(array('min' => 12)); // WHERE occupant2 > 12
     * </code>
     *
     * @see       filterByStudent2()
     *
     * @param     mixed $occupant2 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBedroomQuery The current query, for fluid interface
     */
    public function filterByOccupant2($occupant2 = null, $comparison = null)
    {
        if (is_array($occupant2)) {
            $useMinMax = false;
            if (isset($occupant2['min'])) {
                $this->addUsingAlias(BedroomTableMap::COL_OCCUPANT2, $occupant2['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($occupant2['max'])) {
                $this->addUsingAlias(BedroomTableMap::COL_OCCUPANT2, $occupant2['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BedroomTableMap::COL_OCCUPANT2, $occupant2, $comparison);
    }

    /**
     * Filter the query by a related \Student object
     *
     * @param \Student|ObjectCollection $student The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBedroomQuery The current query, for fluid interface
     */
    public function filterByStudent($student, $comparison = null)
    {
        if ($student instanceof \Student) {
            return $this
                ->addUsingAlias(BedroomTableMap::COL_OCCUPANT1, $student->getId(), $comparison);
        } elseif ($student instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BedroomTableMap::COL_OCCUPANT1, $student->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByStudent() only accepts arguments of type \Student or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Student relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBedroomQuery The current query, for fluid interface
     */
    public function joinStudent($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Student');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Student');
        }

        return $this;
    }

    /**
     * Use the Student relation Student object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \StudentQuery A secondary query class using the current class as primary query
     */
    public function useStudentQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinStudent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Student', '\StudentQuery');
    }

    /**
     * Filter the query by a related \Student object
     *
     * @param \Student|ObjectCollection $student The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBedroomQuery The current query, for fluid interface
     */
    public function filterByStudent2($student, $comparison = null)
    {
        if ($student instanceof \Student) {
            return $this
                ->addUsingAlias(BedroomTableMap::COL_OCCUPANT2, $student->getId(), $comparison);
        } elseif ($student instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BedroomTableMap::COL_OCCUPANT2, $student->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByStudent2() only accepts arguments of type \Student or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Student2 relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBedroomQuery The current query, for fluid interface
     */
    public function joinStudent2($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Student2');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Student2');
        }

        return $this;
    }

    /**
     * Use the Student2 relation Student object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \StudentQuery A secondary query class using the current class as primary query
     */
    public function useStudent2Query($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinStudent2($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Student2', '\StudentQuery');
    }

    /**
     * Filter the query by a related \Unit object
     *
     * @param \Unit|ObjectCollection $unit the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBedroomQuery The current query, for fluid interface
     */
    public function filterByBedroomOne($unit, $comparison = null)
    {
        if ($unit instanceof \Unit) {
            return $this
                ->addUsingAlias(BedroomTableMap::COL_ID, $unit->getBedroom1(), $comparison);
        } elseif ($unit instanceof ObjectCollection) {
            return $this
                ->useBedroomOneQuery()
                ->filterByPrimaryKeys($unit->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBedroomOne() only accepts arguments of type \Unit or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BedroomOne relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBedroomQuery The current query, for fluid interface
     */
    public function joinBedroomOne($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BedroomOne');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BedroomOne');
        }

        return $this;
    }

    /**
     * Use the BedroomOne relation Unit object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UnitQuery A secondary query class using the current class as primary query
     */
    public function useBedroomOneQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBedroomOne($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BedroomOne', '\UnitQuery');
    }

    /**
     * Filter the query by a related \Unit object
     *
     * @param \Unit|ObjectCollection $unit the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBedroomQuery The current query, for fluid interface
     */
    public function filterByBedroomTwo($unit, $comparison = null)
    {
        if ($unit instanceof \Unit) {
            return $this
                ->addUsingAlias(BedroomTableMap::COL_ID, $unit->getBedroom2(), $comparison);
        } elseif ($unit instanceof ObjectCollection) {
            return $this
                ->useBedroomTwoQuery()
                ->filterByPrimaryKeys($unit->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBedroomTwo() only accepts arguments of type \Unit or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BedroomTwo relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBedroomQuery The current query, for fluid interface
     */
    public function joinBedroomTwo($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BedroomTwo');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BedroomTwo');
        }

        return $this;
    }

    /**
     * Use the BedroomTwo relation Unit object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UnitQuery A secondary query class using the current class as primary query
     */
    public function useBedroomTwoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBedroomTwo($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BedroomTwo', '\UnitQuery');
    }

    /**
     * Filter the query by a related \Unit object
     *
     * @param \Unit|ObjectCollection $unit the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBedroomQuery The current query, for fluid interface
     */
    public function filterByBedroomThree($unit, $comparison = null)
    {
        if ($unit instanceof \Unit) {
            return $this
                ->addUsingAlias(BedroomTableMap::COL_ID, $unit->getBedroom3(), $comparison);
        } elseif ($unit instanceof ObjectCollection) {
            return $this
                ->useBedroomThreeQuery()
                ->filterByPrimaryKeys($unit->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBedroomThree() only accepts arguments of type \Unit or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BedroomThree relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBedroomQuery The current query, for fluid interface
     */
    public function joinBedroomThree($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BedroomThree');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BedroomThree');
        }

        return $this;
    }

    /**
     * Use the BedroomThree relation Unit object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UnitQuery A secondary query class using the current class as primary query
     */
    public function useBedroomThreeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBedroomThree($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BedroomThree', '\UnitQuery');
    }

    /**
     * Filter the query by a related \Unit object
     *
     * @param \Unit|ObjectCollection $unit the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBedroomQuery The current query, for fluid interface
     */
    public function filterByBedroomFour($unit, $comparison = null)
    {
        if ($unit instanceof \Unit) {
            return $this
                ->addUsingAlias(BedroomTableMap::COL_ID, $unit->getBedroom4(), $comparison);
        } elseif ($unit instanceof ObjectCollection) {
            return $this
                ->useBedroomFourQuery()
                ->filterByPrimaryKeys($unit->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBedroomFour() only accepts arguments of type \Unit or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BedroomFour relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBedroomQuery The current query, for fluid interface
     */
    public function joinBedroomFour($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BedroomFour');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BedroomFour');
        }

        return $this;
    }

    /**
     * Use the BedroomFour relation Unit object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UnitQuery A secondary query class using the current class as primary query
     */
    public function useBedroomFourQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBedroomFour($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BedroomFour', '\UnitQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBedroom $bedroom Object to remove from the list of results
     *
     * @return $this|ChildBedroomQuery The current query, for fluid interface
     */
    public function prune($bedroom = null)
    {
        if ($bedroom) {
            $this->addUsingAlias(BedroomTableMap::COL_ID, $bedroom->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the bedroom table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BedroomTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BedroomTableMap::clearInstancePool();
            BedroomTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BedroomTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BedroomTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BedroomTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BedroomTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // BedroomQuery
