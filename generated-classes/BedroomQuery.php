<?php

use Base\BedroomQuery as BaseBedroomQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'bedroom' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class BedroomQuery extends BaseBedroomQuery
{

}
