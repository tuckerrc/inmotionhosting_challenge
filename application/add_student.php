<?php

require_once __DIR__."/../vendor/autoload.php";
require_once __DIR__."/../generated-conf/config.php";

$name = $address = $phone = $gender = $student_id = $dob = "";

if($_SERVER["REQUEST_METHOD"] == "POST"){
    $name = clean_input($_POST["name"]);
    $address = clean_input($_POST["address"]);
    $phone = clean_input($_POST['phone']);
    $gender = clean_input($_POST['gender']);
    $student_id = clean_input($_POST['student_id']);
    $dob = clean_input($_POST['dob']);

   
    $student = new Student();
    $student->setName($name);
    $student->setAddress($address);
    $student->setPhone($phone);
    $student->setGender($gender);
    $student->setStudentId($student_id);
    $student->setDob($dob);
    $student->save();

}

function clean_input($value){
    $value = trim($value);
    $value = stripslashes($value);
    $value = htmlspecialchars($value);
    return $value;
}

?>