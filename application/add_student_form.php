<!DOCTYPE html>
<html lang="en">

    <?php include("includes/head.php"); ?>

<body>

    <?php include("includes/nav.php"); ?>
    
	<div class="container">
	    <h1>Add Student</h1>
	    <form method="post" action="application/add_student.php">
		<div class="form-group">
		    <label for="name">Student Name: </label>
		    <input type="name" class="form-control" id="name" name="name">
		</div>
		<div class="form-group">
		    <label for="address">Address: </label>
		    <input type="address" class="form-control" id="address" name="address">
		</div>
		<div class="form-group">
		    <label for="phone">Phone: </label>
		    <input type="phone" class="form-control" id="phone" name="phone">
		</div>
		<div class="form-group">
		    <label for="gender">Gender: </label>
		    <select class="form-control" id="gender" type="gender" name="gender">
			<option value="0">Male</option>
			<option value="1">Female</option>
		    </select>
		</div>
		<div class="form-group">
		    <label for="student_id">Student ID: </label>
		    <input type="student_id" class="form-control" id="student_id" name="student_id">
		</div>
		<div class="form-group">
		    <label for="dob">Date of Birth: </label>
		    <input type="date" class="form-control" id="dob" name="dob">
		</div>
		<div class="form-group"> 
		    <div class="col-sm-10">
			<button type="submit" class="btn btn-default">Submit</button>
		    </div>
		</div>
	    </form>
	    
	    
	</div><!-- /.container -->

	<?php include("includes/footer.php"); ?>

    </body>
</html>
