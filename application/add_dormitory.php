<?php

require_once __DIR__."/../vendor/autoload.php";
require_once __DIR__."/../generated-conf/config.php";

$dorm_building_number = 3; // Need to figure out a way to automate so a duplicate building number is not used. -- Maybe another table in database

for($i = 1; $i <= 4; $i++){
    // Four Floors
    print_r("Floor " . $i ."\n");
    $floor = $i;
    for ($j = 1; $j<= 4; $j++){
        // Four units per floor
        $gender = 0;
        if($j%2 == 0){
            // half of the units are female the other half are male
            $gender = 1;
        }
        $unit = new Unit();
        $unit->setFloorNumber($floor);
        $unit->setDormBuilding($dorm_building_number);
        $unit->setUnitNumber($j);
        $unit->setGender($gender);
        
        print_r('- Unit ' . $j . "\n");
        $bedrooms = [];
        for ($k=1;$k<=4; $k++){
            //four bedrooms per unit
            print_r("-- Bedroom " . $k . "\n");
            $bed = new Bedroom();
            $bed->save();
            $bedrooms[] = $bed;
            
        }
        $unit->setBedroomOne($bedrooms[0]);
        $unit->setBedroomTwo($bedrooms[1]);
        $unit->setBedroomThree($bedrooms[2]);
        $unit->setBedroomFour($bedrooms[3]);
        $unit->save();
    }
}
?>