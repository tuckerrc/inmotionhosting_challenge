-- MySQL dump 10.16  Distrib 10.1.22-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: tucker_chapman
-- ------------------------------------------------------
-- Server version	10.1.22-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bedroom`
--

DROP TABLE IF EXISTS `bedroom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bedroom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `occupant1` int(11) DEFAULT NULL,
  `occupant2` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bedroom_fi_82bbf5` (`occupant1`),
  KEY `bedroom_fi_2bc4ca` (`occupant2`),
  CONSTRAINT `bedroom_fk_2bc4ca` FOREIGN KEY (`occupant2`) REFERENCES `student` (`id`),
  CONSTRAINT `bedroom_fk_82bbf5` FOREIGN KEY (`occupant1`) REFERENCES `student` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bedroom`
--

LOCK TABLES `bedroom` WRITE;
/*!40000 ALTER TABLE `bedroom` DISABLE KEYS */;
INSERT INTO `bedroom` VALUES (1,1,2),(2,NULL,NULL),(3,NULL,NULL),(4,NULL,NULL),(5,3,NULL),(6,NULL,NULL),(7,NULL,NULL),(8,NULL,NULL),(9,NULL,NULL),(10,NULL,NULL),(11,NULL,NULL),(12,NULL,NULL),(13,NULL,NULL),(14,NULL,NULL),(15,NULL,NULL),(16,NULL,NULL),(17,NULL,NULL),(18,NULL,NULL),(19,NULL,NULL),(20,NULL,NULL),(21,NULL,NULL),(22,NULL,NULL),(23,NULL,NULL),(24,NULL,NULL),(25,NULL,NULL),(26,NULL,NULL),(27,NULL,NULL),(28,NULL,NULL),(29,NULL,NULL),(30,NULL,NULL),(31,NULL,NULL),(32,NULL,NULL),(33,NULL,NULL),(34,NULL,NULL),(35,NULL,NULL),(36,NULL,NULL),(37,NULL,NULL),(38,NULL,NULL),(39,NULL,NULL),(40,NULL,NULL),(41,NULL,NULL),(42,NULL,NULL),(43,NULL,NULL),(44,NULL,NULL),(45,NULL,NULL),(46,NULL,NULL),(47,NULL,NULL),(48,NULL,NULL),(49,NULL,NULL),(50,NULL,NULL),(51,NULL,NULL),(52,NULL,NULL),(53,NULL,NULL),(54,NULL,NULL),(55,NULL,NULL),(56,NULL,NULL),(57,NULL,NULL),(58,NULL,NULL),(59,NULL,NULL),(60,NULL,NULL),(61,NULL,NULL),(62,NULL,NULL),(63,NULL,NULL),(64,NULL,NULL),(65,NULL,NULL),(66,NULL,NULL),(67,NULL,NULL),(68,NULL,NULL),(69,NULL,NULL),(70,NULL,NULL),(71,NULL,NULL),(72,NULL,NULL),(73,NULL,NULL),(74,NULL,NULL),(75,NULL,NULL),(76,NULL,NULL),(77,NULL,NULL),(78,NULL,NULL),(79,NULL,NULL),(80,NULL,NULL),(81,NULL,NULL),(82,NULL,NULL),(83,NULL,NULL),(84,NULL,NULL),(85,NULL,NULL),(86,NULL,NULL),(87,NULL,NULL),(88,NULL,NULL),(89,NULL,NULL),(90,NULL,NULL),(91,NULL,NULL),(92,NULL,NULL),(93,NULL,NULL),(94,NULL,NULL),(95,NULL,NULL),(96,NULL,NULL),(97,NULL,NULL),(98,NULL,NULL),(99,NULL,NULL),(100,NULL,NULL),(101,NULL,NULL),(102,NULL,NULL),(103,NULL,NULL),(104,NULL,NULL),(105,NULL,NULL),(106,NULL,NULL),(107,NULL,NULL),(108,NULL,NULL),(109,NULL,NULL),(110,NULL,NULL),(111,NULL,NULL),(112,NULL,NULL),(113,NULL,NULL),(114,NULL,NULL),(115,NULL,NULL),(116,NULL,NULL),(117,NULL,NULL),(118,NULL,NULL),(119,NULL,NULL),(120,NULL,NULL),(121,NULL,NULL),(122,NULL,NULL),(123,NULL,NULL),(124,NULL,NULL),(125,NULL,NULL),(126,NULL,NULL),(127,NULL,NULL),(128,NULL,NULL);
/*!40000 ALTER TABLE `bedroom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `propel_migration`
--

DROP TABLE IF EXISTS `propel_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `propel_migration` (
  `version` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `propel_migration`
--

LOCK TABLES `propel_migration` WRITE;
/*!40000 ALTER TABLE `propel_migration` DISABLE KEYS */;
INSERT INTO `propel_migration` VALUES (1496201211);
/*!40000 ALTER TABLE `propel_migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `gender` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `dob` date NOT NULL,
  `phone` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (1,'Chester Neace','1234 East Ave. New York, NY',0,12345,'1970-01-01','800-123-4567'),(2,'Jarvis Rittle','71 Pilgrim Avenue  Chevy Chase, MD 20815',0,12346,'1995-07-31','202-555-0170'),(3,'Melodi Vecchione','44 Shirley Ave.  West Chicago, IL 60185',1,12347,'1998-02-22','202-555-0145'),(4,'Ludwig Van Beethoven','Germany',0,12348,'1770-12-17','000-000-0000');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unit`
--

DROP TABLE IF EXISTS `unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dorm_building` int(11) NOT NULL,
  `unit_number` int(11) NOT NULL,
  `floor_number` int(11) NOT NULL,
  `gender` int(11) NOT NULL,
  `bedroom1` int(11) DEFAULT NULL,
  `bedroom2` int(11) DEFAULT NULL,
  `bedroom3` int(11) DEFAULT NULL,
  `bedroom4` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `unit_fi_92645e` (`bedroom1`),
  KEY `unit_fi_108e54` (`bedroom2`),
  KEY `unit_fi_72903d` (`bedroom3`),
  KEY `unit_fi_b28104` (`bedroom4`),
  CONSTRAINT `unit_fk_108e54` FOREIGN KEY (`bedroom2`) REFERENCES `bedroom` (`id`),
  CONSTRAINT `unit_fk_72903d` FOREIGN KEY (`bedroom3`) REFERENCES `bedroom` (`id`),
  CONSTRAINT `unit_fk_92645e` FOREIGN KEY (`bedroom1`) REFERENCES `bedroom` (`id`),
  CONSTRAINT `unit_fk_b28104` FOREIGN KEY (`bedroom4`) REFERENCES `bedroom` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unit`
--

LOCK TABLES `unit` WRITE;
/*!40000 ALTER TABLE `unit` DISABLE KEYS */;
INSERT INTO `unit` VALUES (1,1,1,1,0,1,2,3,4),(2,1,2,1,1,5,6,7,8),(3,1,3,1,0,9,10,11,12),(4,1,4,1,1,13,14,15,16),(5,1,1,2,0,17,18,19,20),(6,1,2,2,1,21,22,23,24),(7,1,3,2,0,25,26,27,28),(8,1,4,2,1,29,30,31,32),(9,1,1,3,0,33,34,35,36),(10,1,2,3,1,37,38,39,40),(11,1,3,3,0,41,42,43,44),(12,1,4,3,1,45,46,47,48),(13,1,1,4,0,49,50,51,52),(14,1,2,4,1,53,54,55,56),(15,1,3,4,0,57,58,59,60),(16,1,4,4,1,61,62,63,64),(17,2,1,1,0,65,66,67,68),(18,2,2,1,1,69,70,71,72),(19,2,3,1,0,73,74,75,76),(20,2,4,1,1,77,78,79,80),(21,2,1,2,0,81,82,83,84),(22,2,2,2,1,85,86,87,88),(23,2,3,2,0,89,90,91,92),(24,2,4,2,1,93,94,95,96),(25,2,1,3,0,97,98,99,100),(26,2,2,3,1,101,102,103,104),(27,2,3,3,0,105,106,107,108),(28,2,4,3,1,109,110,111,112),(29,2,1,4,0,113,114,115,116),(30,2,2,4,1,117,118,119,120),(31,2,3,4,0,121,122,123,124),(32,2,4,4,1,125,126,127,128);
/*!40000 ALTER TABLE `unit` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-31 21:07:51
