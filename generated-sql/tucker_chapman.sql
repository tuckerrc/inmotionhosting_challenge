
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- student
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `student`;

CREATE TABLE `student`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `address` VARCHAR(255) NOT NULL,
    `gender` INTEGER NOT NULL,
    `student_id` INTEGER NOT NULL,
    `dob` DATE NOT NULL,
    `phone` VARCHAR(25) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- unit
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `unit`;

CREATE TABLE `unit`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `dorm_building` INTEGER NOT NULL,
    `unit_number` INTEGER NOT NULL,
    `floor_number` INTEGER NOT NULL,
    `gender` INTEGER NOT NULL,
    `bedroom_one` INTEGER,
    `bedroom_two` INTEGER,
    `bedroom_three` INTEGER,
    `bedroom_four` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `unit_fi_605c3f` (`bedroom_one`),
    INDEX `unit_fi_389eff` (`bedroom_two`),
    INDEX `unit_fi_f63a7d` (`bedroom_three`),
    INDEX `unit_fi_afe5be` (`bedroom_four`),
    CONSTRAINT `unit_fk_605c3f`
        FOREIGN KEY (`bedroom_one`)
        REFERENCES `bedroom` (`id`),
    CONSTRAINT `unit_fk_389eff`
        FOREIGN KEY (`bedroom_two`)
        REFERENCES `bedroom` (`id`),
    CONSTRAINT `unit_fk_f63a7d`
        FOREIGN KEY (`bedroom_three`)
        REFERENCES `bedroom` (`id`),
    CONSTRAINT `unit_fk_afe5be`
        FOREIGN KEY (`bedroom_four`)
        REFERENCES `bedroom` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- bedroom
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `bedroom`;

CREATE TABLE `bedroom`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `occupant_one` INTEGER,
    `occupant_two` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `bedroom_fi_2dea9f` (`occupant_one`),
    INDEX `bedroom_fi_d9ffed` (`occupant_two`),
    CONSTRAINT `bedroom_fk_2dea9f`
        FOREIGN KEY (`occupant_one`)
        REFERENCES `student` (`id`),
    CONSTRAINT `bedroom_fk_d9ffed`
        FOREIGN KEY (`occupant_two`)
        REFERENCES `student` (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
