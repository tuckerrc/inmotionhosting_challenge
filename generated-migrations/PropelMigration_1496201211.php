<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1496201211.
 * Generated on 2017-05-31 03:26:51 
 */
class PropelMigration_1496201211
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'tucker_chapman' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE `bedroom` DROP FOREIGN KEY `bedroom_fk_2dea9f`;

ALTER TABLE `bedroom` DROP FOREIGN KEY `bedroom_fk_d9ffed`;

DROP INDEX `bedroom_fi_2dea9f` ON `bedroom`;

DROP INDEX `bedroom_fi_d9ffed` ON `bedroom`;

ALTER TABLE `bedroom`

  CHANGE `occupant_one` `occupant1` INTEGER,

  CHANGE `occupant_two` `occupant2` INTEGER;

CREATE INDEX `bedroom_fi_82bbf5` ON `bedroom` (`occupant1`);

CREATE INDEX `bedroom_fi_2bc4ca` ON `bedroom` (`occupant2`);

ALTER TABLE `bedroom` ADD CONSTRAINT `bedroom_fk_82bbf5`
    FOREIGN KEY (`occupant1`)
    REFERENCES `student` (`id`);

ALTER TABLE `bedroom` ADD CONSTRAINT `bedroom_fk_2bc4ca`
    FOREIGN KEY (`occupant2`)
    REFERENCES `student` (`id`);

ALTER TABLE `unit` DROP FOREIGN KEY `unit_fk_389eff`;

ALTER TABLE `unit` DROP FOREIGN KEY `unit_fk_605c3f`;

ALTER TABLE `unit` DROP FOREIGN KEY `unit_fk_afe5be`;

ALTER TABLE `unit` DROP FOREIGN KEY `unit_fk_f63a7d`;

DROP INDEX `unit_fi_605c3f` ON `unit`;

DROP INDEX `unit_fi_389eff` ON `unit`;

DROP INDEX `unit_fi_f63a7d` ON `unit`;

DROP INDEX `unit_fi_afe5be` ON `unit`;

ALTER TABLE `unit`

  CHANGE `bedroom_one` `bedroom1` INTEGER,

  CHANGE `bedroom_two` `bedroom2` INTEGER,

  CHANGE `bedroom_three` `bedroom3` INTEGER,

  CHANGE `bedroom_four` `bedroom4` INTEGER;

CREATE INDEX `unit_fi_92645e` ON `unit` (`bedroom1`);

CREATE INDEX `unit_fi_108e54` ON `unit` (`bedroom2`);

CREATE INDEX `unit_fi_72903d` ON `unit` (`bedroom3`);

CREATE INDEX `unit_fi_b28104` ON `unit` (`bedroom4`);

ALTER TABLE `unit` ADD CONSTRAINT `unit_fk_92645e`
    FOREIGN KEY (`bedroom1`)
    REFERENCES `bedroom` (`id`);

ALTER TABLE `unit` ADD CONSTRAINT `unit_fk_108e54`
    FOREIGN KEY (`bedroom2`)
    REFERENCES `bedroom` (`id`);

ALTER TABLE `unit` ADD CONSTRAINT `unit_fk_72903d`
    FOREIGN KEY (`bedroom3`)
    REFERENCES `bedroom` (`id`);

ALTER TABLE `unit` ADD CONSTRAINT `unit_fk_b28104`
    FOREIGN KEY (`bedroom4`)
    REFERENCES `bedroom` (`id`);

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'tucker_chapman' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE `bedroom` DROP FOREIGN KEY `bedroom_fk_82bbf5`;

ALTER TABLE `bedroom` DROP FOREIGN KEY `bedroom_fk_2bc4ca`;

DROP INDEX `bedroom_fi_82bbf5` ON `bedroom`;

DROP INDEX `bedroom_fi_2bc4ca` ON `bedroom`;

ALTER TABLE `bedroom`

  CHANGE `occupant1` `occupant_one` INTEGER,

  CHANGE `occupant2` `occupant_two` INTEGER;

CREATE INDEX `bedroom_fi_2dea9f` ON `bedroom` (`occupant_one`);

CREATE INDEX `bedroom_fi_d9ffed` ON `bedroom` (`occupant_two`);

ALTER TABLE `bedroom` ADD CONSTRAINT `bedroom_fk_2dea9f`
    FOREIGN KEY (`occupant_one`)
    REFERENCES `student` (`id`);

ALTER TABLE `bedroom` ADD CONSTRAINT `bedroom_fk_d9ffed`
    FOREIGN KEY (`occupant_two`)
    REFERENCES `student` (`id`);

ALTER TABLE `unit` DROP FOREIGN KEY `unit_fk_92645e`;

ALTER TABLE `unit` DROP FOREIGN KEY `unit_fk_108e54`;

ALTER TABLE `unit` DROP FOREIGN KEY `unit_fk_72903d`;

ALTER TABLE `unit` DROP FOREIGN KEY `unit_fk_b28104`;

DROP INDEX `unit_fi_92645e` ON `unit`;

DROP INDEX `unit_fi_108e54` ON `unit`;

DROP INDEX `unit_fi_72903d` ON `unit`;

DROP INDEX `unit_fi_b28104` ON `unit`;

ALTER TABLE `unit`

  CHANGE `bedroom1` `bedroom_one` INTEGER,

  CHANGE `bedroom2` `bedroom_two` INTEGER,

  CHANGE `bedroom3` `bedroom_three` INTEGER,

  CHANGE `bedroom4` `bedroom_four` INTEGER;

CREATE INDEX `unit_fi_605c3f` ON `unit` (`bedroom_one`);

CREATE INDEX `unit_fi_389eff` ON `unit` (`bedroom_two`);

CREATE INDEX `unit_fi_f63a7d` ON `unit` (`bedroom_three`);

CREATE INDEX `unit_fi_afe5be` ON `unit` (`bedroom_four`);

ALTER TABLE `unit` ADD CONSTRAINT `unit_fk_389eff`
    FOREIGN KEY (`bedroom_two`)
    REFERENCES `bedroom` (`id`);

ALTER TABLE `unit` ADD CONSTRAINT `unit_fk_605c3f`
    FOREIGN KEY (`bedroom_one`)
    REFERENCES `bedroom` (`id`);

ALTER TABLE `unit` ADD CONSTRAINT `unit_fk_afe5be`
    FOREIGN KEY (`bedroom_four`)
    REFERENCES `bedroom` (`id`);

ALTER TABLE `unit` ADD CONSTRAINT `unit_fk_f63a7d`
    FOREIGN KEY (`bedroom_three`)
    REFERENCES `bedroom` (`id`);

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

}